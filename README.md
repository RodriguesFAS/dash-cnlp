# ToolKit NLP DashBoard

![alt text](#)

# Types Install

## Virtual Machine

        cd dash-cnlp run_server.py

#### Using DashBoard

        python 

#### Using Terminal

        pyhton nlp_annotation.py \
        "{
                'remove_extra_spacing': True,
                'morp_type': True,
                'check_grammar': True,
                'srl_cogcomp': True,
                'gazzetter': True,
                'shape': True,
                'word_tokenization': True,
                'dependencies_parsing': True,
                'remove_stop_words': True,
                'tokenization_sentences': True,
                'removing_special_characters': True,
                'stemming': True,
                'semafor': True,
                'ner_corenlp': True,
                'shallow_parse_cogcomp': True,
                'true_case': True,
                'dataset': 'dataset_test_1_sentence.txt',
                'parts_of_speech': True,
                'ner_spacy': True,
                'lemmatization': True,
                'length': True,
                'wsd_freeling': True,
                'svo_srl_freeling': True,
                'wsd_supwsd': True,
        }"


## Install Computer

#### Config permitions script

        sudo chmod +x install.sh

#### Execult script

        ./install.sh


# Docker NLP DashBoard

#### Download Image
        
        docker pull rodriguesfas/dash-cnlp:0.0.4

#### Run Image

        docker run -i -t -p 5000:5000 rodriguesfas/dash-cnlp:0.0.4 /bin/bash

#### Run NLP DashBoard
        
        cd /home/dash-cnlp && python run_server.py

#### Open Browser

        http://127.0.0.1:5000/



# License

[MIT License](https://github.com/rodriguesfas/LexSeDi/blob/master/LICENSE)

# Credits

[Alane](https://github.com/AlaneL)

[Rinaldo](#)

[RodriguesFAS](https://github.com/rodriguesfas)

[João Nascimento](https://github.com/JNMarcos)