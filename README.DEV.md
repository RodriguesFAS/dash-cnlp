
# ToolKit NLP DashBoard

![alt text](#)

## Anaconda

        source activate dash-cnlp

## Generaterd Docker

        docker-compose up -d

## Update Docker

        docker-compose up --build

## Delete Chache

        docker system prune -a


## Update - Force Recreate Imagem

        docker-compose up --force-recreate --build
        docker image prune -f



docker run -i -t -p 5000:5000 dash-cnlp_web /bin/bash
