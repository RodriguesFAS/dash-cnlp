# To build the image: docker build -t rodriguesfas/deep_nlp_dashboard:1.0.0 .
# Run: docker run -i -t -p 5000:5000 rodriguesfas/deep_nlp_dashboard /bin/bash

# Selected INSTALL SO
FROM ubuntu
# FROM debian:jessie

LABEL Description="Deep NLP DashBoard | RodriguesFAS - franciscosouzaacer@gmail.com"

ENV TOOLNLP=/home/tool-nlp/

# INTALL ALL.
RUN apt-get update -y && apt-get upgrade -y && \
    # INSTALL JAVA
        apt-get install -y software-properties-common && \
        add-apt-repository ppa:webupd8team/java && \
        apt-get update && \
        echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
        apt-get install -y oracle-java8-installer && \
    # INSTALL UTILS (if debian)
        #apt install -y utils && \
    # INSTALL UNZIP
        apt-get install -y unzip && \
    # INSTALL LANG
        apt-get install language-pack-ru-base -y && locale-gen en_US en_US.UTF-8 && \ 
    # INSTALL DEPENDECY FREELING
        apt-get install -y build-essential && \
        apt-get install -y cmake && \
        apt-get install -y swig && \
        apt-get install -y libboost-all-dev && \
        apt-get install -y libicu-dev && \ 
        apt-get install -y zlib1g-dev && \
    # CREATE DIR.
        mkdir "$TOOLNLP" && \
    # ENTER DIR TOOL-NLP
        cd "$TOOLNLP" && \
    # DOWNLOAD FREELING
        # path: home/tool-nlp
        apt-get install -y wget && \
        wget https://github.com/TALP-UPC/FreeLing/releases/download/4.1/FreeLing-4.1.tar.gz && \
        tar -vzxf FreeLing-4.1.tar.gz && \
        rm -r FreeLing-4.1.tar.gz && \
        # path: home/tool-nlp/FreeLing-4.1
        cd FreeLing-4.1 && \
        mkdir build && \
        # path: home/tool-nlp/FreeLing-4.1/build
        cd build && \
    # INSTALL API PYTHON2 FREELING
        cmake .. -DPYTHON2_API=ON && \
    # INSTALL FREELING
        make -j 4 install && \
    # DOWNLOAD AND INSTALL STANFORD CORENLP
        # path: home/tool-nlp
        cd "$TOOLNLP" && \
        wget http://nlp.stanford.edu/software/stanford-corenlp-full-2018-10-05.zip && \
        unzip stanford-corenlp-full-2018-10-05.zip && \
        rm -r stanford-corenlp-full-2018-10-05.zip && \
    # DOWNLOAD, INSTALL AND CONFIG SEMAFOR
        # path: home/tool-nlp
        cd "$TOOLNLP" && \
        wget https://github.com/Noahs-ARK/semafor/archive/master.zip && \
        unzip master.zip && \
        rm -r master.zip && \
        mv semafor-master semafor && \
    # CONFIG FILE SEMAFOR
        # path: home/tool-nlp/semafor/bin
        cd semafor/bin && \
        rm -r config.sh && \
        echo -e '#!/bin/sh \
        \n\nexport BASE_DIR="/home/tool-nlp" \
        \nexport SEMAFOR_HOME="${BASE_DIR}/semafor" \
        \nexport CLASSPATH=".:${SEMAFOR_HOME}/target/Semafor-3.0-alpha-04.jar" \
        \nexport JAVA_HOME_BIN="/usr/lib/jvm/java-8-oracle/bin" \
        \nexport MALT_MODEL_DIR="${BASE_DIR}/semafor/models/semafor_malt_model_20121129" \
        \nexport TURBO_MODEL_DIR="{BASE_DIR}/semafor/models/turbo_20130606" \
        \n\necho "Environment variables:" \
        \necho "SEMAFOR_HOME=${SEMAFOR_HOME}" \
        \necho "CLASSPATH=${CLASSPATH}" \
        \necho "JAVA_HOME_BIN=${JAVA_HOME_BIN}" \
        \necho "MALT_MODEL_DIR=${MALT_MODEL_DIR}" \
        '>> config.sh && \
    # SEMAFOR DOWNLOAD MODELS 
        # path: home/tool-nlp/semafor
        cd .. && \
        mkdir -p models && \
        cd models && \
        # path: home/tool-nlp/semafor/models
        wget http://www.ark.cs.cmu.edu/SEMAFOR/semafor_malt_model_20121129.tar.gz && \
        tar -vzxf semafor_malt_model_20121129.tar.gz && \
        rm -r semafor_malt_model_20121129.tar.gz && \
    # SEMAFOR INSTALL MAVEN 
        apt-get install maven -y && \
    # SEMAFOR INSTALL PACKAGE
        # path: home/tool-nlp/semafor
        cd .. && \ 
        mvn package && \
    # INSTALL PIP
        apt-get install python-pip -y && \
    # INSTALL DEPENDECY PROJECT PYTHON REQUIREMENTS
        pip install -U spacy && \
        python -m spacy download en && \
        pip install pathlib && \
        pip install xmlformatter && \
        pip install flask_socketio && \
        pip install nltk && \
        python -c "import nltk; nltk.download('all')" && \
        pip install stanfordcorenlp && \
        pip install supwsd &&\
    # INSTALL GIT
        apt-get install git -y && \
    # CLONE PROJECT
        # path: home/
        cd ~ && cd /home && \
        git clone https://RodriguesFAS@bitbucket.org/RodriguesFAS/dash-cnlp.git && \
    # CLEAR INSTALL
        apt-get clean -y

WORKDIR /home/dash-cnlp

CMD python run_server.py && /bin/bash
