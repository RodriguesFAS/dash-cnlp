#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    File: run_server.py
    Description: Run Server
    Date: 07/11/2018
"""

import os
import json
import datetime
import config.settings as config

import modules.statistics as stat
import modules.myxmlformatter as myxmlform

from werkzeug import secure_filename
from flask_socketio import SocketIO, send
from ConfigParser import SafeConfigParser
from flask import Flask, request, render_template, send_from_directory, redirect, url_for, jsonify, flash, send_file

import nlp_annotation



basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'

socketio = SocketIO(app)


# ================================================== Upload file dataset.
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'csv', 'xml'])



def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']



@app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)



def dated_url_for(endpoint, **values):
    if endpoint == 'js_static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path, 'static/js', filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    elif endpoint == 'css_static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path, 'static/css', filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)



@app.route('/css/<path:filename>')
def css_static(filename):
    return send_from_directory(app.root_path + '/static/css/', filename)



@app.route('/js/<path:filename>')
def js_static(filename):
    return send_from_directory(app.root_path + '/static/js/', filename)



@app.route('/uploadajax', methods=['POST'])
def upldfile():
    if request.method == 'POST':
        files = request.files['file']
        if files and allowed_file(files.filename):
            filename = secure_filename(files.filename)
            updir = os.path.join(basedir, 'dataset/input/')
            files.save(os.path.join(updir, filename))
            file_size = os.path.getsize(os.path.join(updir, filename))
            return jsonify(name=filename, size=file_size)



@socketio.on('message')
def handleMessage(msg):
	print('Message: ' + msg)
	send(msg, broadcast=True)



def get_files(_dir):
    files = []
    for file in os.listdir('dataset/'+_dir):
        item = { "filename": file, "path_file": basedir+'/dataset/'+_dir+'/'+file }
        files.append(item)
    return files



# ================================================== Web Pages

@app.route("/dataset_list", methods=["GET", "POST"])
def dataset_list():
    return render_template('dataset_list.html', title="Deep | Dataset List", files=get_files('input'))



@app.route("/dataset_view", methods=["GET", "POST"])
def dataset_view():
    if request.method == 'GET':
        path_file = request.args.get('path')
        dataset = open(path_file, 'r').read()
        return render_template('dataset_view.html', title="Deep | Dataset View", dataset=dataset)



@app.route("/dataset_statistics", methods=["GET", "POST"])
def dataset_statistics():
    if request.method == 'GET':
        path_file = request.args.get('path')
        dataset = stat.loar_data(path_file)

        rb, jj, vb, nn, outhers = stat.split_pos_tag(dataset)
        rb_lemma, jj_lemma, vb_lemma, nn_lemma, outhers_lemma = stat.split_pos_tag(dataset, True)
        tts = stat.total_tokens_sentences(dataset)

        return render_template(
                'dataset_statistics.html', title="Deep | Dataset Statistics", path_file=path_file,
                ans=stat.average_sentences(dataset), 
                ns=stat.total_sentences(dataset), 
                tt=stat.total_tokens(dataset),
                rb=rb, jj=jj, vb=vb, nn=nn, outhers=outhers,
                rb_lemma=rb_lemma, jj_lemma=jj_lemma, vb_lemma=vb_lemma, nn_lemma=nn_lemma, outhers_lemma=outhers_lemma,
                number_sent = tts,
                freq = set(tts)
            )



@app.route("/dataset_download", methods=["GET", "POST"])
def dataset_download():
    if request.method == 'GET':
        path_file = request.args.get('path')
        name_file = path_file.split('/')
        print name_file[6]
    return send_file(path_file, attachment_filename=name_file[6], mimetype="application/txt", as_attachment=True)



@app.route("/dataset_delete", methods=["GET", "POST"])
def dataset_delete():
    if request.method == 'GET':
        path_file = request.args.get('path')
        os.remove(path_file)
    return render_template('dataset_list.html', title="Deep | Dataset List", files=get_files('input'))



@app.route('/', methods=["GET", "POST"])
def annotation():

    option_selected = {}

    if request.method == 'POST':

        ## step 1
        option_selected['dataset'] = request.form.get('name_dataset')

        ## step 2
        if request.form.get('sentences_splits'):
            option_selected['sentences_splits'] = True
        else:
            option_selected['sentences_splits'] = False

        if request.form.get('word_tokenization'):
            option_selected['word_tokenization'] = True
        else:
            option_selected['word_tokenization'] = False
            
        if request.form.get('removing_special_characters'):
            option_selected['removing_special_characters'] = True
        else:
            option_selected['removing_special_characters'] = False
            
        if request.form.get('remove_extra_spacing'):
            option_selected['remove_extra_spacing'] = True
        else:
            option_selected['remove_extra_spacing'] = False
            
        if request.form.get('check_grammar'):
            option_selected['check_grammar'] = True
        else:
            option_selected['check_grammar'] = False

        ##  step 3
        if request.form.get('lemmatization'):
            option_selected['lemmatization'] = True
        else:
            option_selected['lemmatization'] = False
            
        if request.form.get('stemming'):
            option_selected['stemming'] = True
        else:
            option_selected['stemming'] = False
            
        if request.form.get('length'):
            option_selected['length'] = True
        else:
            option_selected['length'] = False
            
        if request.form.get('parts_of_speech'):
            option_selected['parts_of_speech'] = True
        else:
            option_selected['parts_of_speech'] = False
            
        if request.form.get('ner_corenlp'):
            option_selected['ner_corenlp'] = True
        else:
            option_selected['ner_corenlp'] = False
            
        if request.form.get('ner_spacy'):
            option_selected['ner_spacy'] = True
        else:
            option_selected['ner_spacy'] = False
            
        if request.form.get('shape'):
            option_selected['shape'] = True
        else:
            option_selected['shape'] = False
            
        if request.form.get('morph_type'):
            option_selected['morph_type'] = True
        else:
            option_selected['morph_type'] = False
            
        if request.form.get('true_case'):
            option_selected['true_case'] = True
        else:
            option_selected['true_case'] = False
            
        if request.form.get(''):
            option_selected['gazzetter'] = True
        else:
            option_selected['gazzetter'] = False
            
        if request.form.get('shallow_parsing_cogcomp'):
            option_selected['shallow_parsing_cogcomp'] = True 
        else:
            option_selected['shallow_parsing_cogcomp'] = False
            
        if request.form.get('dependencies_parsing'):
            option_selected['dependencies_parsing'] = True
        else: 
            option_selected['dependencies_parsing'] = False
            
        if request.form.get('wsd_freeling'):
            option_selected['wsd_freeling'] = True
        else:
            option_selected['wsd_freeling'] = False
            
        if request.form.get('wsd_supwsd'):
            option_selected['wsd_supwsd'] = True
        else:
            option_selected['wsd_supwsd'] = False
            
        if request.form.get('semafor'):
            option_selected['semafor'] = True
        else:
            option_selected['semafor'] = False
            
        if request.form.get('srl_cogcomp'):
            option_selected['srl_cogcomp'] = True
        else:
            option_selected['srl_cogcomp'] = False
            
        if request.form.get('svo_srl_freeling'):
            option_selected['svo_srl_freeling'] = True
        else: 
            option_selected['svo_srl_freeling'] = False

        # Test
        '''
        if option_selected:
            print option_selected
            flash(option_selected)
        '''

        result = nlp_annotation.run(option_selected) # option_selected = type:dict

        if result:
            flash(result)

    return render_template('annotation.html', title="Deep | Annotation", dataset=get_files('input'))



@app.route("/annotation_output", methods=["GET", "POST"])
def annotation_output():
    return render_template('annotation_output.html', title="Deep | List Annotation", files=get_files('output'))



@app.route("/annotation_view_xml", methods=["GET", "POST"])
def annotation_view_xml():
    if request.method == 'GET':
        path_file = request.args.get('path')
        xml_string = myxmlform.string_xml_formatter(open(path_file, 'r').read())
    return render_template('annotation_view_xml.html', title="Deep | View Annotation XML", annotation=xml_string)



@app.route("/annotation_view_csv", methods=["GET", "POST"])
def annotation_view_csv():
    if request.method == 'GET':
        path_file = request.args.get('path')
        xml_string = myxmlform.string_xml_formatter(open(path_file, 'r').read())
    return render_template('annotation_view_csv.html', title="Deep | View Annotation CSV", annotation=xml_string)



@app.route("/annotation_download", methods=["GET", "POST"])
def annotation_download():
    if request.method == 'GET':
        path_file = request.args.get('path')
        name_file = path_file.split('/')
    return send_file(path_file, attachment_filename=name_file[6], mimetype="application/xml", as_attachment=True)



@app.route( "/annotation_delete", methods=["GET", "POST"] )
def annotation_delete():
    if request.method == 'GET':
        path_file = request.args.get('path')
        os.remove(path_file)
        return render_template('annotation_output.html', title="Deep | List Annotation", files=get_files('output'))



@app.route("/about")
def about():
    return render_template('about.html', title="Deep | About", current_time=datetime.datetime.now())



@app.route( "/save_configurations", methods=["POST"] )
def save_configurations():
    config = SafeConfigParser()
    config.read('settings.cfg')
    config.add_section('telegram')

    if request.method == 'POST':
        if request.form.get('send_msg'):
            send_msg = 'True'
        else:
            send_msg = 'False'

        config.set('telegram', 'send_msg', send_msg)
        config.set('telegram', 'telegram_token', str(request.form.get('telegram_token')) )
        config.set('telegram', 'telegram_chat_id', str(request.form.get('telegram_chat_id')) )

        with open(basedir+'/config/settings.cfg', 'w') as file:
            config.write(file)

        return True



def get_configurations():
    config = SafeConfigParser()
    config.read(basedir+'/config/settings.cfg')

    send_msg = config.get('telegram', 'send_msg')
    telegram_token = config.get('telegram', 'telegram_token')
    telegram_chat_id = config.get('telegram', 'telegram_chat_id')
    
    return send_msg, telegram_token, telegram_chat_id


@app.route('/configurations', methods=["GET", "POST"])
def configurations():
    send_msg, telegram_token, telegram_chat_id = get_configurations()

    if send_msg == 'True':
        send_msg = 'checked'
    else:
        send_msg = ''

    return render_template('configurarions.html', title="Deep | Configurations", send_msg=send_msg, telegram_token=telegram_token, telegram_chat_id=telegram_chat_id )



@app.route("/changelog")
def changelog():
    return render_template('changelog.html', title="Deep | Changelog")


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=5000, debug=True)