# -*- coding: utf-8 -*-

"""
    Software:
    Description:
    Date: 09/12/2018
"""

import os
import nltk

from nltk.stem import WordNetLemmatizer
from nltk.tokenize import sent_tokenize, word_tokenize


def loar_data(path):
    '''
        Loard dataset.
        Carrega o dataset.
    '''
    data_temp = []
    
    with open(path, 'r') as file:
        #sent_tokenize_list = sent_tokenize(file.read())
        for sentence in file:
            sentence = sentence.replace('.', '') # remove pontuaction.
            data_temp.append(sentence) 
        return data_temp



def total_sentences(dataset):
    '''
        Calculate the number of sentences in the dataset.
        Calcular a quantidade de sentenças do dataset.

        @param dataset
        @return amount
    '''
    return len(dataset)


def total_tokens(dataset):
    '''
        Calculates the total number of tokens in the dataset.
        Calcula o total de tokens no dataset.

        @param dataset
        @return amount
    '''
    cont_tokens = 0

    for sentence in dataset:
        cont_tokens += len(word_tokenize(sentence))
    
    return cont_tokens


def average_sentences(dataset):
    '''
        Calculates the average size of the phrase by number of tokens.
        Calcula o tamanho médio das frase por número de tokens.

        @param dataset
        @return number of tokens.
    '''
    t_tokens = total_tokens(dataset)
    t_sentences = total_sentences(dataset)

    # verifica se os divisores são maiores que 0
    if t_tokens > 0 and t_sentences > 0:
        return t_tokens / t_sentences
    else:
        return 0


def total_tokens_sentences(dataset):
    '''
        Calculates the number of tokens per sentence.
        Calcula o número de tokens por sentença.
    '''
    cont_tokens_sentence = []
    
    for sentence in dataset:
        cont_tokens_sentence.append(len(word_tokenize(sentence)))

    return cont_tokens_sentence 



def split_pos_tag(_dataset, _lemma=False):
    '''
        Calculates the frequency number of tokens per class, in different lists.
        Calcula o número de frequência de tokens por classes, em listas diferentes.

        @param
            _dataset
            _lemma

        @return
    '''
    rb_temp = []
    jj_temp = []
    vb_temp = []
    nn_temp = []
    outhers_temp = []
    all_temp = []
    
    for line in _dataset:
        
        if _lemma is False:
            words_tokens = word_tokenize(line)
        else:
            words_tokens = []
            lemmatizer = WordNetLemmatizer()

            for word in word_tokenize(line):
                if word.isalpha():
                    words_tokens.append(lemmatizer.lemmatize(word))

        for word in nltk.pos_tag(words_tokens):
            if word[0].isalpha():
                # RB = "Adverb", JJ = "Adjective", VB = "Verb", NN = "Noun"
                if word[1] == 'RB':
                    rb_temp.append(word[1]+'_lemma')
                elif word[1] == 'JJ':
                    jj_temp.append(word[1]+'_lemma')
                elif word[1] == 'VB':
                    vb_temp.append(word[1]+'_lemma')
                elif word[1] == 'NN':
                    nn_temp.append(word[1]+'_lemma')
                else:
                    outhers_temp.append('OUTHERS') # define identification all.
    
    return len(rb_temp), len(jj_temp), len(vb_temp), len(nn_temp), len(outhers_temp)
