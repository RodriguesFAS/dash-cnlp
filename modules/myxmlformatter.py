"""
    Date: 09/12/2018
    Lib.: https://pypi.org/project/xmlformatter/
"""

import xmlformatter

def string_xml_formatter(xml_string):
    """
        @param xml_string
            string xml no formatted.
        
        @return 
            string xml formatted.
    """
    formatter = xmlformatter.Formatter(indent="1", indent_char="\t", encoding_output="ISO-8859-1", preserve=["literal"])
    return formatter.format_string(xml_string)