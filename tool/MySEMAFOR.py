'''
    File MySEMAFOR.py
    Description: SEMAFOR is a frame-semantic parser.
    Data: 03/11/2018
    Page: http://www.cs.cmu.edu/~ark/SEMAFOR/
    Github: https://github.com/Noahs-ARK/semafor
'''

import os
import sys
import json
import config.settings as config
import util.loaddata as ld


PATH_SEMAFOR = config.PATH['path_dir_semafor']
PATH_SCRIPT = './bin/runSemafor.sh'
PATH_RAW_INPUT = config.PATH['path_dir_temp'] +'/'+ config.FILE['tmp_in_semafor']
PATH_RAW_OUTPUT = config.PATH['path_dir_temp'] +'/'+ config.FILE['tmp_out_semafor']


def raw_input(sentence):
    '''
        Saves the sentence that will be processed in a text file.
        @param sentence
    '''
    temp_file = open(PATH_RAW_INPUT, 'w')
    temp_file.write(sentence)
    temp_file.close()

def raw_output():
    '''
        Retrieves output file generated with processing results.
        @retun data json.
    '''
    with open(PATH_RAW_OUTPUT, 'r') as fp:
         obj = json.load(fp)
    return obj

def get_parse(sentence, threads=1):
    '''
        Execute the SEMAFOR.

        @param theads (inform the number of threads that you want to execute.)
    '''

    # salve sentence in file input.
    raw_input(sentence)

    # check if an output file exists and exclude.
    os.system('cd '+ PATH_SEMAFOR +' && [ -e '+ PATH_RAW_OUTPUT +' ] && rm '+ PATH_RAW_OUTPUT)

    # processes a new file.
    os.system('cd '+ PATH_SEMAFOR +' && '+ PATH_SCRIPT +' '+ PATH_RAW_INPUT +' '+ PATH_RAW_OUTPUT +' '+ str(threads))

    return raw_output()


if __name__ == '__main__':
    '''
        Tests single
    '''
    print get_parse('Do you want me to hold off until I finish July and August ?', 4)