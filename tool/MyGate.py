"""
    Class: MyGate
    Description:
    Data: 20/09/2018
"""

import os
import config.settings as config

from requests.structures import CaseInsensitiveDict


class MyGate(object):
    '''
        Interface Access Category Gazetteer Gate
    '''

    gazzetter_dict = CaseInsensitiveDict({})

    def __init__(self):
        self.load_files(config.PATH['path_dir_gate'])

    def load_files(self, _path):
        """
        """
        for file in os.listdir(_path):
            if file.endswith('.lst'):
                category = file.split('.')
                line = open(_path + file).read()
                for word in line.split('\n'):
                    if word != '':
                        self.gazzetter_dict.setdefault(word, []).append(category[0])

    def format_set(self, _data):
        '''
        '''
        return ', '.join([s for s in set(_data)])

    def get_category(self, _token):
        """
            _token: input word.
            return: output category is word.
        """
        if self.gazzetter_dict.get(_token):
            return self.format_set(set(self.gazzetter_dict.get(_token)))
        else:
            return 'null'
