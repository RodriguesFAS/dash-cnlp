#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Software: MyVerbNet
    Description: 
    Date: 16/09/2018
    GuideLines: 
        http://verbs.colorado.edu/verb-index/VerbNet_Guidelines.pdf
"""

import json
from nltk.corpus import verbnet as vn


class MyVerbNet(object):
    """
        Interface Access VerbNet.
    """

    def get_statistics(self):
        """
            Verbnet statistics.
            @return {lemmas, wordnetids, classids}
        """
        data = {
            "lemmas": len(vn.lemmas()),
            "wordnetids": len(vn.wordnetids()),
            "classids": len(vn.classids())
        }
        return json.loads(json.dumps(data))

    def get_classids_to_lemma(self, _lemma):
        """
            Selecting classids based lemma.

                @param _lemma
                    ex. input: take

                @return 
                    ex. output: ['bring-11.3', 'characterize-29.2', 'convert-26.6.2', 'cost-54.2', 'fit-54.3', 'performance-26.7-2', 'steal-10.5']
        """
        return ', '.join(vn.classids(lemma=_lemma))

    def get_classid_to_wordnetid(self, _wordnetid):
        """
            Selecting classids based wordnetid.

                @param _wordnetid
                    ex. input: lead%2:38:01

                @return 
                    ex. output: ['accompany-51.7']
        """
        return ' '.join(vn.classids(wordnetid=_wordnetid))

    def get_classids_to_fileid(self, _fileid):
        """
            Selecting classids based fileid.

                @param _fileid
                    ex. input: approve-77.xml

                @return 
                    ex. output: ['approve-77']
        """
        return ' '.join(vn.classids(fileid=_fileid))

    def get_classids_to_classid(self, _classid):
        """
            Selecting classids based classid.

                @param _classid
                    ex. input: admire-31.2

                @return 
                    ex. output: ['admire-31.2-1']
        """
        return ' '.join(vn.classids(classid=_classid))

    def get_verb(self, _sense_wn):
        """
            @param _sense_wn
            @return
        """
        return _sense_wn.split('%')

    def get_id(self, _classids):
        res = _classids.split('-', 1)
        return res[1]

    def get_wordnetids(self, _vcclass=None):
        """
            Return a list of all wordnet identifiers that appear in any class, or in classid if specified.
            Retorna uma lista de todos os identificadores wordnet que aparecem em qualquer class ou em um classid especificado.

            @param _vcclass
                Ex. input: 

            @return
                Ex. output: 
        """
        if _vcclass is None:
            return vn.wordnetids()
        else:
            return vn.wordnetids(_vcclass)

    def get_lemmas(self, _classid=None):
        """
            Return a list of all verb lemmas that appear in any class, or in the 'classid' if specified.
            Devolve uma lista de todos os lemmas verbais que aparecem em qualquer class ou no classid especificado.

            @param _classid
                ex. input: give-13.1-1

            @return
                ex. output: [give, hock, rent, sell, lease, pawn]
        """
        if _classid is None:
            return vn.lemmas()
        else:
            return ', '.join(vn.lemmas(_classid))

    def get_interval_lemmas(self, _start, _end):
        """
            @return interval lemmas excludent end.
        """
        return ', '.join(vn.lemmas()[_start:_end])

    def get_vnclass_tree(self, _classid):
        """
            @param _classid

            @return 
        """
        return vn.vnclass(_classid)

    def get_vnclass(self, fileid_or_classid):
        """
            Given a VerbNet class, this method returns VerbNet frames The members returned are:
                1) Example
                2) Description
                3) Syntax
                4) Semantics

            @param fileid_or_classid

            @return
        """
        v = vn.vnclass(fileid_or_classid)
        return ' '.join([t.attrib['type'] for t in v.findall('THEMROLES/THEMROLE/SELRESTRS/SELRESTR')])

    def get_frames(self, _vcclass):
        """
            Given a VerbNet class, this method returns VerbNet frames The members returned are:
            Dada uma class VerbNet, esse método retorna VerbNet membros frames: 
                1) Example
                2) Description
                3) Syntax
                4) Semantics

            @param _vcclass
            @return
        """
        return vn.frames(_vcclass)

    def get_subclasses(self, _vcclass):
        """
            Returns subclass ids, if any exist Given a VerbNet class, this method returns subclass ids (if they exist) in a list of strings.
            Retorna IDs de subclasses, se existir alguma, dado uma class VerbNet.

            @param _vcclass
                Ex. input: admire-31.2

            @retunr
                Ex. output: ['admire-31.2-1']
        """
        return ', '.join(vn.subclasses(_vcclass)).strip()

    def get_themroles(self, _classid):
        """
            See the Verbnet documentation, or the Verbnet files, for information about the structure of this xml. 
            As an example, we can retrieve a list of thematic roles for a given Verbnet class:

            @param _classid
                ex. input: admire-31.2

            @return
                ex. output: ['Theme', 'Experiencer', 'Predicate']
        """

        data = []
        v_classid = vn.vnclass(_classid)

        for themrole in v_classid.findall('THEMROLES/THEMROLE'):
            item = themrole.attrib['type'], [
                '[%(Value)s%(type)s]' % selrestr.attrib for selrestr in themrole.findall('SELRESTRS/SELRESTR')]
            data.append(item)

        return data

    def get_pprint(self, _classid):
        """
            The Verbnet corpus also provides a variety of pretty printing functions that can
             be used to display the xml contents in a more concise form. The simplest such 
             method is pprint():

            @param _classid
                Ex. input: admire-31.2 

            @return
                Ex. output:
                    admire-31.2
                        Subclasses: admire-31.2-1
                        Members: admire adore appreciate bear bewail cherish deify
                            disbelieve esteem exalt fancy favor grudge idolize miss prefer
                            affirm reaffirm prize respect relish revere savor stand support
                            tolerate treasure trust value venerate worship abhor deplore
                            distrust dread envy execrate lament loathe mourn pity resent rue
                            believe suffer
                        Thematic roles:
                            * Theme
                            * Experiencer[+animate]
                            * Predicate
                        Frames:
                            Basic Transitive
                            Example: The tourists admired the paintings.
                            Syntax: NP[Experiencer] VERB NP[Theme]
                            Semantics:
                                * emotional_state(E, Emotion, Experiencer)
                                * in_reaction_to(E, Theme)
                            NP-PP (Possessor Object, Predicate-PP)
                            Example: I admired him for his honesty.
                            Syntax: NP[Experiencer] VERB NP[Theme] PREP[for] NP[Predicate]
                            Semantics:
                                * emotional_state(E, Emotion, Experiencer)
                                * in_reaction_to(E, Theme)
                            NP-PP (Predicate Object, Possessor-PP)
                            Example: I admired the honesty in him.
                            Syntax: NP[Experiencer] VERB NP[Predicate] PREP[in] NP[Theme]
                            Semantics:
                                * emotional_state(E, Emotion, Experiencer)
                                * in_reaction_to(E, Theme)
                            S
                            Example: The children liked that the clown had a red nose.
                            Syntax: NP[Experiencer] VERB NP[Theme +that_comp]
                            Semantics:
                                * emotional_state(E, Emotion, Experiencer)
                                * in_reaction_to(E, Theme)
                            ING-SC/BE-ING
                            Example: I loved writing.
                            Syntax: NP[Experiencer] VERB NP[Theme +be_sc_ing]
                            Semantics:
                                * emotional_state(E, Emotion, Experiencer)
                                * in_reaction_to(E, Theme)
                            POSSING
                            Example: I loved him writing novels.
                            Syntax: NP[Experiencer] VERB NP[Theme +poss_ing]
                            Semantics:
                                * emotional_state(E, Emotion, Experiencer)
                                * in_reaction_to(E, Theme) 
        """
        return vn.pprint(_classid)

    def get_members(self, _vnclass):
        """
            @param _vnclass
                Ex. input: admire-31.2

            @return
                Ex. output: admire, adore, appreciate, bear, bewail, cherish, 
                            deify, disbelieve, , esteem, exalt, fancy, favor, 
                            grudge, idolize, miss, prefer, affirm, reaffirm, , prize, respect, relish, revere, savor, stand, support, tolerate, treasure, , trust, value, venerate, worship, abhor, deplore, distrust, dread, envy, , execrate, lament, loathe, mourn, pity, resent, rue, believe, suffer
        """
        return vn.pprint_members(_vnclass).replace('\n', '').replace("Members: ", '').replace(" ", ", ").replace(' ,', '')
    
    def get_pprint_frames(self, _vnclass):
        """
            @param _vnclass
                Ex. input: give-13.1-1

            @return
                Ex. output:
                    NP-PP (Asset-PP)
                    Example: He leased the car for $200 a week.
                    Syntax: NP[Agent] VERB NP[Theme] PREP[for at] NP[Asset]
                    Semantics:
                        * has_possession(start(E), Agent, Theme)
                        * has_possession(end(E), ?Recipient, Theme)
                        * has_possession(start(E), ?Recipient, Asset)
                        * has_possession(end(E), Agent, Asset)
                        * transfer(during(E), Theme)
                    NP-PP-PP (Recipient-PP Asset-PP)
                    Example: I leased the car to my friend for $5 a month.
                    Syntax: NP[Agent] VERB NP[Theme] PREP[to] NP[Recipient] PREP[at for on] NP[Asset]
                    Semantics:
                        * has_possession(start(E), Agent, Theme)
                        * has_possession(end(E), Recipient, Theme)
                        * has_possession(start(E), Recipient, Asset)
                        * has_possession(end(E), Agent, Asset)
                        * transfer(during(E), Theme)
                    NP-PP (Asset-PP)
                    Example: I leased him the car for $250 a month.
                    Syntax: NP[Agent] VERB NP[Recipient] NP[Theme] PREP[at for on] NP[Asset]
                    Semantics:
                        * has_possession(start(E), Agent, Theme)
                        * has_possession(end(E), Recipient, Theme)
                        * has_possession(start(E), Recipient, Asset)
                        * has_possession(end(E), Agent, Asset)
                        * transfer(during(E), Theme)
        """
        return vn.pprint_frames(_vnclass)