#!/usr/bin/env python
# -*- coding: utf-8 -*-
# encoding: utf-8

"""
    Software: CogComp
    Description: 
    Date: 25/07/2018

    Demo Online: 
        http://nlp.cogcomp.org

    GET data JSON:
        http://macniece.seas.upenn.edu:4001/annotate?text=%22The%20boy%27s%20gift%20was%20to%20the%20girl.%22&views=SRL_NOM
"""

import config.settings as config
import requests
import subprocess
import util.log as log


class MyCogComp(object):

    path_dir_cogcomp_nlp = config.PATH['path_dir_cogcomp_nlp']

    IP = config.CONFIG['server']
    PORT = config.CONFIG['cogcomp_port']

    service_view_names = IP + ':' + PORT + '/viewNames'
    service_annotate = IP + ':' + PORT + '/annotate'

    headers = {'accept': 'application/json', }

    def start_server_local(self):
        subprocess.call('cd ' + self.path_dir_cogcomp_nlp +
                        ' && pipeline/scripts/runWebserver.sh', shell=True)

    def names_functions(self):
        response = requests.get(self.service_view_names)
        #print 'URL: ', response.url
        return response.text

    def pipeline_local(self, _sent, _view):
        params = {'text': '"' + _sent + '"', 'views': _view}
        response = requests.get(self.service_annotate, params=params)
        #print 'URL: ', response.url
        return response.text

    def server_web(self, _sent, _views):
        server_url = "http://macniece.seas.upenn.edu:4001/annotate"
        params = {'text': '"' + _sent + '"', 'views': _views}
        valid = False
        
        while not valid:
            try:
                response = requests.get(server_url, headers=self.headers, params=params)
                valid = True
                return response.json()
                #print 'URL: ', response.url
            except Exception as err:
                log.logging.error("CogComp Exception occurred: "+_sent, exc_info=True)
        
        

