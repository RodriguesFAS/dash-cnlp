#!/usr/bin/env python
# -*- coding: utf-8 -*-
# encoding: utf-8

"""
    Software: MyARK
    Description: 
    Date: 28/08/2018

    Site: 
        http://www.cs.cmu.edu/~ark/
    
    Demo Online:
        http://demo.ark.cs.cmu.edu/parse

    GET data JSON:
        http://demo.ark.cs.cmu.edu/parse/api/v1/parse?sentence=The+boy+gave+the+frog+to+the+girl+.   
"""

import config.settings as config
import requests
import subprocess
import util.log as log


class MyARK(object):
    """
        Interface Access ARK.
    """

    server_url = "http://demo.ark.cs.cmu.edu/parse/api/v1/parse"
    headers = { 'accept': 'application/json', }

    def get_parse(self, _sent):
        """
            @param _sent
                Ex. input: The boy gave the frog to the girl .

            @return 
                Ex. output:

            TODO adcionar um tray a essa função para controlar o erro.
        """
        valid = False
        params = { 'sentence': _sent }
        
        while not valid:
            try:
                response = requests.get(self.server_url, headers=self.headers, params=params)
                valid = True
                return response.json()
            except Exception as err:
                log.logging.error("ARK Exception occurred:"+_sent, exc_info=True)

