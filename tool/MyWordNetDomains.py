"""
    Class: MyWordNetDomains
    Description:
    Data: 06/10/2018
    Site Oficial: http://wndomains.fbk.eu/
"""

import os
import config.settings as config


class MyWordNetDomains(object):
    """
        Interface de Access WordNet Domains 3.2
    """

    wnd_dict = {}

    def __init__(self):
        self.load_files(config.PATH['path_dir_wnd'])

    def load_files(self, _path):
        """
            Loard data in dictionary.
        """
        for file in os.listdir(_path):
            if file.endswith('.txt'):
                line = open(_path + file).read()
                for l in line.split('\n'):
                    if l != '':
                        synset, category = l.split(',', 1)
                        self.wnd_dict.setdefault(synset, []).append(category)

    def get_category(self, _synset):
        """
            @param _synset
                Ex. input: 00522811-r

            @return 
                Ex. output:
        """
        if self.wnd_dict.get(_synset):
            return ', '.join(set(self.wnd_dict.get(_synset)))
        else:
            return 'null' 
