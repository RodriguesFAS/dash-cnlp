#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Class: MyXWordNetDomains
    Description:
    Date: 04/10/2018
    Fonte: https://github.com/bogdan-ivanov/wnext
"""
from __future__ import print_function

from nltk.corpus import wordnet as wn
from nltk.corpus.reader.wordnet import Synset

from collections import defaultdict

import os
import config.settings as config


class MyXWordNetDomains:
    """
        Corpus Reader for X Wordnet Domains.
    """

    def __init__(self, abspath=None):
        self.domain_dict = defaultdict(lambda: defaultdict(list))

        if abspath is None:
            file_folder = os.path.dirname(os.path.realpath(__file__))
            abspath = os.path.join(file_folder, config.FILE['xwnd'])

        domain_files = [f for f in os.listdir(abspath) if f.endswith('.ppv')]

        self.domains = []

        for idx, filename in enumerate(domain_files):
            domain_name, _ = filename.split('.')

            with open(os.path.join(abspath, filename), 'r') as df:
                for line in df:
                    synset_idx, relevancy = line.split()
                    relevancy = float(relevancy)
                    offset, pos = synset_idx.split('-')
                    offset = int(offset)

                    self.domain_dict[pos][offset].append(
                        (domain_name, relevancy))

            self.domains.append(domain_name)

    def synset(self, offset, pos=wn.NOUN):
        """
            Get domains of synset by offset and part of speech. Returs list of tuples (domain, relevancy) sorted by relevancy.
            Obter domínios de synset por offset e parte da fala. Lista de retorno de tuplas (domínio, relevância), ordenados por relevância.
        """
        try:
            return sorted(self.domain_dict[str(pos)][offset], key=lambda item: -item[1])
        except:
            return None

    def get_domains(self, _synset):
        """
            @param
                Ex. input:
                        submarine.n.01
                        book.n.01
                        algorithm.n.01
            @return 
                Ex. output: 
                    [('nautical', 0.00106965), ('military', 0.000348186), ('optics', 0.00018319), ('atomic_physic', 0.000130893), ('sub', 9.32671e-05)]
                    [('roman_catholic', 0.00319282), ('publishing', 0.00285319), ('philology', 0.00133688), ('literature', 0.00092217), ('pedagogy', 0.000497682)]
                    [('mathematics', 0.000466441), ('computer_science', 0.000268545), ('statistics', 4.22156e-05), ('pure_science', 3.65301e-05), ('grammar', 3.14656e-05)]

        """
        code = wn.synset(_synset)
        return code.domains()[:5]


def wordnetextensions_domains(self):
    """
        Method that decorates the Synset class, to get access to WordnetDomains.
        Método que decora a classe Synset, para obter acesso ao WordnetDomains.
    """
    if not hasattr(self.__class__, 'wndomains_reader'):
        self.__class__.wndomains_reader = MyXWordNetDomains()

    return self.__class__.wndomains_reader.synset(self.offset(), self.pos())


# Decorate the Synset class with sentiment values from SentiWordnet
Synset.domains = wordnetextensions_domains
