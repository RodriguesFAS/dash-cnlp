#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Software: MyPyWSD
    Description: 
    Date: 04/07/2018
    Github:
        https://github.com/alvations/pywsd

    TODO: Servidor em Java, implementar futuramente essa ferramenta offline.
"""

from pywsd import disambiguate
from pywsd.similarity import max_similarity as maxsim

class MyPyWSD(object):
    '''
        Interface from PyWSD.
    '''

    def disambiguate(self, _sent):
        '''
        '''
        return disambiguate(_sent)

    def disambiguate_max(self, _sent):
        '''
        '''
        return disambiguate(_sent, algorithm=maxsim, similarity_option='wup', keepLemmas=True)