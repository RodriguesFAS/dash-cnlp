#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Class: MySupWSD 
    Description:
    Date: 12/09/2018
    
	Github: 
		https://github.com/SI3P/supWSD
	
	PyPI: 
		https://pypi.org/project/supwsd/
	
	Documentation: 
		https://supwsd-supwsdweb.1d35.starter-us-east-1.openshiftapps.com/supwsdweb/api-python.jsp
"""

import json

from supwsd.wsd import SupWSD

class MySupWSD(object):

	

	def disambiguate(self, _sent):
		data_temp = []
		for sense in SupWSD().senses(_sent):
			item = {
				"word": sense.word,
				"lemma": sense.lemma,
				"pos": sense.pos,
				"sense": sense.key(), 
			}
			data_temp.append(item)
		return data_temp 