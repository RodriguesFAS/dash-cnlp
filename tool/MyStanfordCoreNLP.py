#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Software: My Stanford NLP
    Description:
    Date: 03/07/2018
    Github: https://github.com/Lynten/stanford-corenlp
"""

import re
import json
import config.settings as config

from stanfordcorenlp import StanfordCoreNLP


class MyStanfordCoreNLP(object):
    '''
    '''

    path = config.PATH['path_dir_stanford_corenlp']

    '''
        annotators: tokenize, ssplit, pos, lemma, ner, *regexner, sentiment, 
                    truecase, parse, depparse, dcoref, relation, natlog, *quote

        * = no found.
    '''

    def word_tokenize(self, sent):
        '''
            Tokenize the sentence.
        '''
        nlp = StanfordCoreNLP(self.path)
        result = nlp.word_tokenize(sent)
        nlp.close()
        return result   

    def pos(self, token):
        '''
            POS
            @param token
            @return pos
        '''
        nlp = StanfordCoreNLP(self.path)
        result = nlp.pos_tag(token)
        nlp.close()
        return result[:2]

    def ner(self, sent):
        '''
        '''
        nlp = StanfordCoreNLP(self.path)
        result = nlp.ner(sent)
        nlp.close()
        return result

    def constituency_parsing(self, sent):
        '''
        '''
        nlp = StanfordCoreNLP(self.path)
        result = nlp.parse(sent)
        nlp.close()
        return re.sub(' +', ' ', result.replace('\n', ''))

    def dependency_parsing(self, sent):
        '''
        '''
        nlp = StanfordCoreNLP(self.path)
        result = nlp.dependency_parse(sent)
        nlp.close()
        return result

    def annotate_token(self, token):
        '''
        '''
        props = {
            'annotators': 'tokenize',
            'pipelineLanguage': 'en', 
            'outputFormat': 'json'
            }
        nlp = StanfordCoreNLP(self.path)
        result = json.loads(nlp.annotate(token, properties=props))
        nlp.close()
        return result

    def annotate_ssplit(self, text):
        '''
        '''
        props = {
            'annotators': 'ssplit', 
            'pipelineLanguage': 'en', 
            'outputFormat': 'json'
            }
        nlp = StanfordCoreNLP(self.path)
        result = json.loads(nlp.annotate(text, properties=props))
        nlp.close()
        return result

    def annotate_pos(self, sent):
        '''
        '''
        props = {
            'annotators': 'pos',
            'pipelineLanguage': 'en', 
            'outputFormat': 'json'
            }
        nlp = StanfordCoreNLP(self.path)
        result = json.loads(nlp.annotate(sent, properties=props))
        nlp.close()
        return result

    def annotate_main(self, sent):
        '''
            Make note of the sentence according to the functions selected in the properties.
            Realizar anotação da sentença conforme as funções selecionadas na properties.

            @param sent
            @return json
        '''
        props = {
            'annotators': 'tokenize, ssplit, pos, lemma, ner, parse, depparse, truecase',
            'pipelineLanguage': 'en',
            'outputFormat': 'json'
        }
        
        nlp = StanfordCoreNLP(self.path)
        # result = json.loads(nlp.annotate(sent, properties=self.props))
        result = json.loads(nlp.annotate(sent, properties=props))
        nlp.close()
        return result
