'''
    File: __init__
    Description: 
    Date: 25/07/2018
'''

from LexSeDi import LexSeDi

from MyStanfordCoreNLP import MyStanfordCoreNLP
from MySpaCy import MySpaCy
from MyNLTK import MyNLTK
from MyCogComp import MyCogComp

from MyVerbNet import MyVerbNet
from MyGate import MyGate
from MyWordNet import MyWordNet
from MyWordNetDomains import MyWordNetDomains
from MyXWordNetDomains import MyXWordNetDomains
from MyAdimenSUMO import MyAdimenSUMO

from MySupWSD import MySupWSD