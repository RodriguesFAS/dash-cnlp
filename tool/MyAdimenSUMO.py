"""
    Class: MyAdimenSUMO
    Description:
    Data: 06/10/2018
    Site Oficial: http://adimen.si.ehu.es/web/adimensumo
"""

import re
import os
import config.settings as config
import util.pprocessing as pp


class MyAdimenSUMO(object):
    """
        Mapping Access Interface Adimen SUMO.
    """

    dict_mapping = {}

    def __init__(self):
        self.load_files(config.PATH['path_dir_asumo'])

    def load_files(self, _path):
        """
            Loads the data into a dictionary structure.
        """
        for file in os.listdir(_path):
            if file.endswith('.txt'):
                raw_data = open(_path + file).read()
                for sentence in raw_data.split('\n'):
                    if sentence != '':
                        synset, category = sentence.split(',', 1)
                        self.dict_mapping.setdefault(synset, []).append(category)

    def get_category(self, _synset):
        """
            Selects the classes of a given synset

            @param _synset
                Ex. input: 02772310-v

            @return
                Ex. output: ('Combustion',subsumption)
        """
        if self.dict_mapping.get(_synset):
            return ''.join(set(self.dict_mapping.get(_synset)))
        else:
            return 'null'
    