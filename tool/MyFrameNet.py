#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Class: MyFrameNet
    
    Description:
        FrameNet é um banco de dados léxico inglês, legivel para homens e máquinas, baseado 
        na anotação de exemplos de como as palavras sçao usadas em textos reais. É baseado na 
        teoria do significado Frame Semantics.

    Date: 15/08/2018
    
    Turotial base:
        http://www.nltk.org/howto/framenet.html
        http://naacl.org/naacl-hlt-2015/tutorial-framenet-data/FrameNetAPI.pdf
"""


from nltk.corpus import framenet as fn


class MyFrameNet(object):
    """
    """

    def list_frames(self):
        """
            @return lisr all Frames in FrameNet.
        """
        return fn.frames()

    def get_frames(self, _token):
        """
            @param _token
                ex. input: noise

            @return
                ex. output: [<frame ID=801 name=Cause_to_make_noise>, <frame ID=60 name=Motion_noise>, …] 
        """
        return fn.frames(_token)

    def get_list_frames(self, _token):
        """
            @param _token
                ex. input: noise

            @return
                ex. output: ['Cause_to_make_noise', 'Motion_noise', 'Make_noise', ‘Communication_noise']
        """
        return [f.name for f in fn.frames(_token)]

    def get_list_frames_plus(self, _token):
        """
            @param _token
                ex. input: noise

            @return
                ex. output: ['Cause_to_make_noise', 'Noise_makers', 'Motion_noise', 'Make_noise', 'Communication_noise']
        """
        return [f.name for f in fn.frames('(?i)'+_token)]

    def get_frame(self, _frame):
        """
            @param _frame
                ex. input: Noise_makers

            @return
                ex. output: 
                    frame (1017): Noise_makers

                    [definition]
                    The Noise_maker is an artifact used to produce sound, especially
                    for musical effect. 'The church bells rang' 'The car alarm went
                    off again' 'I have never played an acoustic guitar.' 'Each
                    artist personally owns a Steinway and has chosen to perform on
                    the Steinway piano professionally'

                    [semTypes] 0 semantic types

                    [frameRelations] 1 frame relations <Parent=Artifact -- Inheritance -> Child=Noise_makers>

                    [lexUnit] 10 lexical units alarm.n (11243), bell.n (10211), cello.n (10217), drum.n (10216), 
                    guitar.n (10210), piano.n (10213), rattle.n (10214), saxophone.n (10218), siren.n (10212), 
                    xylophone.n (10215)

                    [FE] 8 frame elements
                        Core: Noise_maker (6043)
                        Peripheral: Creator (6045), Ground (6050), Material (6049),
                        Name (6047), Time_of_creation (6046), Type (6048), Use (6044)

                    [FEcoreSets] 0 frame element core sets
        """
        return fn.frame(_frame)

    def get_frame_relations(self, _type):
        """
            @param _type
                Ex. input: Causative_of'

            @return
        """
        return fn.frame_relations(type=_type)

    def get_frame_json(self, _frame):
        """
            @param
            @return
        """
        f = fn.frame(_frame)
        f.FE
        f.FE[_frame].definition
        f.lexUnit.keys()

        return
