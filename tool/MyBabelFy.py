#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Software: MyBabelFy
    Description: O BabelFy é baseado no BabelNet i sua recursos do FrameNet, WordNet, BD Pedia, Wikipedia, YAGO.
    Date: 02/08/2018
"""


import config.settings as config
import urllib2
import urllib
import json
import gzip

from StringIO import StringIO


class MyBabelFy(object):

    lang = config.LANG['babelfy']
    key = config.KEYS['babelfy']
    service_url = 'https://babelfy.io/v1/disambiguate'

    def disambiguate(self, _sent):
        params = {'text': _sent, 'lang': self.lang, 'key': self.key}

        url = self.service_url + '?' + urllib.urlencode(params)
        request = urllib2.Request(url)
        request.add_header('Accept-encoding', 'gzip')
        response = urllib2.urlopen(request)

        if response.info().get('Content-Encoding') == 'gzip':
            file = gzip.GzipFile(fileobj=StringIO(response.read()))
            return json.loads(file.read())
