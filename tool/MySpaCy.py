#!/usr/bin/env python
# # -*- coding: utf-8 -*-

"""
    Software: MySpacCy
    Description:
    Date: 04/07/2018
"""

import os
import spacy
import subprocess

from spacy import displacy
from collections import Counter


class MySpaCy(object):

    nlp = spacy.load('en_core_web_sm')

    def text(self, data):
        ''' 
            texto original
            https://spacy.io/usage/linguistic-features#tokenization
        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for token in data:
            data = token.text
        return data

    def idx(self, data):
        '''

        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for token in data:
            data = token.idx
        return data

    def lemma(self, data):
        '''
            A forma básica da palavra.
        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for token in data:
            data = token.lemma_
        return data

    def is_punct(self, data):
        '''
        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for token in data:
            data = token.is_punct
        return data

    def is_space(self, data):
        '''
        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for token in data:
            data = token.is_space
        return data

    def shape(self, data):
        '''
            A forma da palavra, letras maiúsculas, pontuação, dígitos.
        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for token in data:
            data = token.shape_
        return data

    def pos(self, data):
        '''
            A tag simples da parte da fala.
        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for token in data:
            data = token.pos_
        return data

    def tag(self, data):
        '''
            A tag detalhada da parte da fala.
        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for token in data:
            data = token.tag_
        return data

    def start(self, data):
        '''
            indice do início da entidade no documento.
        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for ent in data.ents:
            data = ent.start_char
        return data

    def end(self, data):
        '''
            indice do fim da entidade no documento.
        '''
        data = self.nlp(unicode(data).decode('utf8'))
        for ent in data.ents:
            data = ent.end_char
        return data

    def get_ner(self, _data):
        '''
            NER

            Documentation: 
                https://spacy.io/usage/linguistic-features#section-named-entities
                https://spacy.io/usage/linguistic-features#101
                https://spacy.io/api/annotation#named-entities
        '''
        doc = self.nlp(unicode(_data).decode('utf8'))
        for ent in doc.ents:
            doc = ent.label_
        return doc
    
    def get_dep(self, _data):
        '''
            Dependência sintática, isto é, a relação entre os termos.
            
            @param _data
            @return
        '''
        doc = self.nlp(unicode(_data).decode('utf8'))
        for token in doc:
            doc = token.dep_
        return doc

    def dependencies_input(self, data):
        '''
        '''
        data_temp = []
        data = self.nlp(unicode(data).decode('utf8'))
        for token in data:
            aux = token.dep_, token.head.text, token.text
            data_temp.append(aux)
        data_temp = str(data_temp).replace(
            '[', '(ROOT ').replace(']', ')').replace(',', '')
        return data_temp

    def chunk_verb(self, _sent, out_option):
        '''
        '''
        import textacy
        sentence = self.nlp(unicode(_sent).decode('utf8'))
        pattern = r'<VERB>?<ADV>*<VERB>+'
        lists = textacy.extract.pos_regex_matches(sentence, pattern)
        data_temp = []
        for list in lists:
            data_temp.append(list.text)
        if out_option == 0:
            return " ".join(data_temp)
        if out_option == 1:
            return data_temp

    def bio(self, _tokens):
        '''
            B - o token é o começo de uma entidade.
            I - o token está dentro de uma entidade.
            O - o token está fora de uma entidade

            https://spacy.io/usage/linguistic-features#accessing
        '''
        _tokens = self.nlp(unicode(_tokens).decode('utf8'))
        # tokens_pos_tag = [(X.text, X.label_) for X in _tokens.ents]
        bio = [(X, X.ent_iob_, X.ent_type_) for X in _tokens]
        return bio