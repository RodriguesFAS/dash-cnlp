#!/usr/bin/ python
# -*- coding: utf-8 -*-

'''
    Class: MyDBPediaSpotligth
    Description:
    Date: 03/09/2018
'''

import config.settings as config
import requests


class MyDBPediaSpotligth(object):

    lang = config.LANG['bdpediaspotligth']
    service_url = 'http://api.dbpedia-spotlight.org/'+lang+'/annotate'
    headers = {'accept': 'application/json', }
    data = [('confidence', '0.35'), ]  # no implementation ..

    def disambiguate_ws_html(self, _sent):
        params = (('text', _sent), )
        response = requests.get(self.service_url, params=params)
        return response.text

    def disambiguate_ws_json(self, _sent):
        params = (('text', _sent), )
        response = requests.get(
            self.service_url, headers=self.headers, params=params)
        return response.json()
