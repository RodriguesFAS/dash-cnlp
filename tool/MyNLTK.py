#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Software: MyNLTK
    Description:
    Date: 04/07/2018
"""

import nltk

import sys
sys.path.append('../')

import util.pprocessing as pp

from nltk.corpus import wordnet as WN
from nltk.corpus import stopwords

from nltk.stem import PorterStemmer
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import sent_tokenize, word_tokenize

from nltk.chunk import conlltags2tree, tree2conlltags
from pprint import pprint


class MyNLTK(object):

    def sent_tokenize(self, _text):
        return nltk.tokenize.sent_tokenize(_text)

    def word_tokenize(self, _sent):
        return nltk.tokenize.word_tokenize(_sent)

    def pos_tagging(self, _tokens):
        return [nltk.pos_tag(token) for token in _tokens]

    def stemming_porter(self, _data):
        stemming_porter = PorterStemmer()
        return stemming_porter.stem(_data)

    def stemming_snowball(self, _data):
        stemming_snowball = SnowballStemmer('english')
        return stemming_snowball.stem(_data)

    def ne_chunk_sents(self, _pos_tagging):
        return  nltk.ne_chunk_sents(_pos_tagging)

    def ne_chunk(self, _tagged_tokens):
        return nltk.ne_chunk(_tagged_tokens)

    # incomplet.
    def spell_checker(self, _sent):
        
        for word in nltk.word_tokenize(pp.remove_punct(_sent.lower())):
            strip = word.rstrip()
            if not WN.synsets(strip):
                if strip in set(stopwords.words('english')):
                    print("No mistakes :" + word)
                else:
                    print("Wrong spellings : " + word)
            else:
                print("No mistakes :" + word)

        return "null"

    def bio(self, _sent):
        pattern = 'NP: {<DT>?<JJ>*<NN>}'
        cp = nltk.RegexpParser(pattern)
        cs = cp.parse(nltk.pos_tag(_sent))
        return tree2conlltags(cs)

    def wsd_lesk(self, _sent):
        return 'null'