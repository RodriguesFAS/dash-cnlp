#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Software: LexSeDi
    Description:
    Date: 03/07/2018
"""

import config.settings as config

class LexSeDi(object):

    def out(self):
        print '\        SORRY              /'
        print '\                         /'
        print '\    Something went     /'
        print "]         wrong!      [    ,'|"
        print "]                     [   /  |"
        print "]___               ___[ ,'   |"
        print "]  ]\             /[  [ |:   |"
        print "]  ] \           / [  [ |:   |"
        print "]  ]  ]         [  [  [ |:   |"
        print "]  ]  ]__     __[  [  [ |:   |"
        print "]  ]  ] ]\ _ /[ [  [  [ |:   |"
        print "]  ]  ] ] (#) [ [  [  [ :===='"
        print "]  ]  ]_].nHn.[_[  [  ["
        print "]  ]  ]  HHHHH. [  [  ["
        print ']  ] /   `HH("N  \ [  ['
        print ']__]/     HHH  "  \[__['
        print "]         NNN         ["
        print ']         N/"         ['
        print ']         N H         ['
        print "/          N           \""
        print "/           q,            \""
        print "/                           \""

    def unique_path(self, directory, name_pattern):
        counter = 0
        while True:
            counter += 1
            path = str(directory) + str(name_pattern.format(counter))
            if not path.exists():
                return path

    def tree(self, directory):
        print '+ {directory}'
        for path in sorted(directory.rglob('*')):
            depth = len(path.relative_to(directory).parts)
            spacer = '    ' * depth
            print '{spacer}+ {path.name}'

    def get_morph_type(self, _token):
        '''
            Check shape morphology of the token.
            Checar se uma string contém apenas letras, contém apenas números, checar se a string é alfa-numérica, 
            ou seja, contém apenas letras e números, sem caracteres especiais.

            https://wiki.python.org.br/ManipulandoStringsComPython

            @param _token
                ex. input: car
                ex. input: 123
                ex. input: LexSeDi123
                ex. input: LexSeD#123

            @return
                ex. output: word
                ex. output: num
                ex. output: alph_num
                ex. output: simb
        '''
        '''
        import re

        if re.match('^[a-zA-Z]*$', _token):
            return 'word'
        elif re.match('^[0-9]*$', _token):
            return 'num'
        elif re.match('^[a-zA-Z0-9_]+$', _token):
            return 'alph_num'
        else:
            return 'simb'
        '''
        if _token.isalpha():
            return 'word'
        elif _token.isdigit():
            return 'num'
        elif _token.isalnum():
            return 'alph_num'
        else:
            return 'simb'
