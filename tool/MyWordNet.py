#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

"""
    Software: MyWordNet
    Date: 03/08/2018

    Tutorial: http://www.nltk.org/howto/wordnet.html
"""

from nltk.corpus import wordnet as wn
from itertools import chain


class MyWordNet(object):
    """
        WordNet Access Interface
    """

    def format_synset(self, _data):
        '''
        '''
        return str(_data).replace("Synset('", '').replace("')", '').replace("'", '').replace('(', '').replace(')', '').replace('[', '').replace(']', '')

    def format_set(self, _data):
        '''
        '''
        return ', '.join([s for s in set(_data)])

    def get_offset_pos_key(self, _offset_pos_key):
        """
            @param _offset_pos_key
                ex. input: 01629000-v

            @return
                ex.: output: render.v.04
        """
        # divide the code into two parts.
        offset, pos_key = _offset_pos_key.split('-')
        return self.format_synset(wn._synset_from_pos_and_offset(pos_key, int(offset)))

    def get_synset(self, _synset):
        """
            @param _synset
                ex. input: dog.n.01

            @return
                ex. output: [dog.n.01, frump.n.01, dog.n.03, cad.n.01, frank.n.02, pawl.n.01, andiron.n.01, chase.v.01]
        """
        return wn.synset(_synset)

    def get_synsets(self, _token):
        """
            @param _token
                ex. input: dog

            @return
                ex. output: [dog.n.01, frump.n.01, dog.n.03, cad.n.01, frank.n.02, pawl.n.01, andiron.n.01, chase.v.01]
        """
        return self.format_synset(wn.synsets(_token)[:5])

    def get_synsets_tree(self, _token):
        """
            @param _token
                ex. input: car

            @return
                ex. output: 
        """
        data = []
        for synset in wn.synsets(_token):
            data.append(synset.lemma_names)
        return data

    def get_lemma_key(self, _lemma_key):
        """
            @param _lemma_key
                ex. input: give%2:32:01::

            @return 
                ex. output: give.v.05
        """
        # receives a lemm key as input and returns the corresponding synset.
        return self.format_synset(wn.lemma_from_key(_lemma_key).synset())

    # def synsets(self, _token, _pos):
    #    return wn.synsets(_token, _pos=wn.VERB)

    def get_definition(self, _synset):
        """
            @param _synset
                Ex. input: dog.n.01

            @return
                Ex. output: 
                    a member of the genus Canis (probably descended from the common wolf) 
                    that has been domesticated by man since prehistoric times; occurs in many breeds
        """
        return wn.synset(_synset).definition().replace('"', '').replace("'", '')

    def get_examples(self, _synset, _one=False):
        """
            @param _synset
                ex. input: give.v.05

            @param _one
                ex. input: True
                or
                ex. input: False

            @return 
                if _one = True
                ex. output: Don't pay him any mind

            @return 
                if _one = False
                ex. output: Don't pay him any mind, give the orders, Give him my best regards, pay attention
        """
        if _one is True:
            return wn.synset(_synset).examples()[0]
        else:
            return wn.synset(_synset).examples()
    def get_hypernyms(self, _synset):
        """
            Hypernyms: são conceitos mais gerais. "eat" tem dois deles, o que mostra que o WordNet 
                       não está organizado em uma árvore simples de conceitos. Isso torna a 
                       verificação de ancestrais comuns um pouco mais complexa, mas representa
                       os conceitos de maneira mais realista.

            @param _synset
                ex. input: eat.v.01

            @return 
                ex. output: [consume.v.02, eat.v.02]
        """
        return self.format_synset(wn.synset(_synset).hypernyms())

    def get_all_hypernyms(self, _synset):
        """
            Get the set of hypernyms of the hypernym of the synset etc.
            Nouns can have multiple hypernyms, so we can't just create a depth-sorted list.
        """
        hypernyms = []
        self._recurse_all_hypernyms(_synset, hypernyms)
        return set(hypernyms)

    def _recurse_all_hypernyms(self, _synset, all_hypernyms):
        synset_hypernyms = wn._synset.hypernyms()
        if synset_hypernyms:
            all_hypernyms += synset_hypernyms
            for hypernym in synset_hypernyms:
                self._recurse_all_hypernyms(hypernym, all_hypernyms)

    def get_hyponyms(self, _synset, _start=None, _end=None):
        """
            Hipónimos: São conceitos mais específicos. "eat" tem vários. 
                       Estes podem ter outros hypernyms além de "eat" e 
                       podem ter vários hyponyms.

            @param _synset
                ex. input: dog.n.01

            @return 
                ex. output: [basenji.n.01, corgi.n.01, cur.n.01, dalmatian.n.02, 
                             great_pyrenees.n.01, griffon.n.02, hunting_dog.n.01, 
                             lapdog.n.01, leonberg.n.01, mexican_hairless.n.01, 
                             newfoundland.n.01, pooch.n.01, poodle.n.01, pug.n.01, 
                             puppy.n.01, spitz.n.01, toy_dog.n.01, working_dog.n.01]

        """
        if _start is None and _end is None: # get all.
            return self.format_synset(wn.synset(_synset).hyponyms())
        elif _start != None and _end != None: # get interval.
            return self.format_synset(wn.synset(_synset).hyponyms()[_start:_end])
        elif _start != None and _end is None: # get one especific.
            return self.format_synset(wn.synset(_synset).hyponyms()[:_start])

    def get_synonyms(self, _token, _limit=None):
        """
            @param _token
                ex. input: drink

            @return
                ex. output: toast, tope, drinkable, swallow, pledge, crapulence, 
                            drink, imbibe, booze, beverage, drunkenness, drink_in, fuddle, 
                            potable, drinking, wassail, boozing, salute, deglutition

            https://stackoverflow.com/questions/15730473/wordnet-find-synonyms
            https://stackoverflow.com/questions/19258652/how-to-get-synonyms-from-nltk-wordnet-python/32718824
        """
        synonyms = []

        for syn in wn.synsets(_token):
            for lemma in syn.lemmas(): 
                synonyms.append(str(lemma.name())) # syn.lemma_names
        
        if _limit is None:
            return ', '.join(set(synonyms))
        else:
            return ', '.join(set(synonyms[:_limit]))


    def get_antonyms(self, _token, _limit=None):
        """
            @param _token
                ex. input: man

            @return 
                ex. output: woman, civilian

            https://www.geeksforgeeks.org/get-synonymsantonyms-nltk-wordnet-python/
        """
        antonyms = []

        for syn in wn.synsets(_token):
            for lemma in syn.lemmas():
                if lemma.antonyms():
                    antonyms.append(lemma.antonyms()[0].name())

        if _limit is None:
            return ', '.join(set(antonyms))
        else: 
            return ', '.join(set(antonyms[:_limit]))

    def get_member_holonyms(self, _synset):
        """
            @param _synset
                ex. input: dog.n.01

            @return
                ex. output: 
        """
        result = wn.synset(_synset).member_holonyms()
        return str(result).replace("Synset('", "").replace("')", "")

    def get_entailments(self, _synset):
        """
            @param _synset
                ex. input: eat.v.01

            @return 
                ex. output: 
        """
        return self.format_synset(wn.synset(_synset).entailments())

    def get_hypernym_paths(self, _synset, _quant=None):
        """
            @param1 _synset
                Ex. input: car.n.01

            @param2 _quant
                None || 0

            @return
                Ex. output None: All
                Ex. output 0: Selected
        """
        synset = wn.synset(_synset)
        paths = synset.hypernym_paths()

        if _quant is None:
            data = str(paths).replace('Synset', '')
            return data
        else:
            data_temp = []
            for synset in paths[_quant]:
                item = synset.name
                item = str(item).replace(
                    "<bound method Synset.name of Synset('", "").replace("')>", '').replace(',', '')
                data_temp.append(item)
                # reverse the list from the bottom up
                list_invert = data_temp[::-1]
            # top 5, excluding the first, is the item itself.
            return ', '.join(list_invert[1:6])
