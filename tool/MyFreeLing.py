#! /usr/bin/python
# -*- coding: utf-8 -*-

'''
    File: MyFreeling
    Description:
    Date: 30/08/2018
    Documentation: https://talp-upc.gitbook.io/freeling-4-1-user-manual/v/master/
    Tutorial: https://talp-upc.gitbook.io/freeling-tutorial/
'''

import sys
import json
import config.settings as config
import pyfreeling


def fill_config(lang, ipath):
    '''
        Load an ad-hoc set of configuration options
    '''
    cfg = pyfreeling.config_options()

    # Language of text to process
    cfg.Lang = lang

    # path to language specific data
    lpath = ipath + "/share/freeling/" + cfg.Lang + "/"

    # Tokenizer configuration file
    cfg.TOK_TokenizerFile = lpath + "tokenizer.dat"

    # Splitter configuration file
    cfg.SPLIT_SplitterFile = lpath + "splitter.dat"

    # Morphological analyzer options
    cfg.MACO_Decimal = "."
    cfg.MACO_Thousand = ","
    cfg.MACO_LocutionsFile = lpath + "locucions.dat"
    cfg.MACO_QuantitiesFile = lpath + "quantities.dat"
    cfg.MACO_AffixFile = lpath + "afixos.dat"
    cfg.MACO_ProbabilityFile = lpath + "probabilitats.dat"
    cfg.MACO_DictionaryFile = lpath + "dicc.src"
    cfg.MACO_NPDataFile = lpath + "np.dat"
    cfg.MACO_PunctuationFile = lpath + "../common/punct.dat"
    cfg.MACO_ProbabilityThreshold = 0.001

    # Sense annotator and WSD config files
    cfg.SENSE_ConfigFile = lpath + "senses.dat"
    cfg.UKB_ConfigFile = lpath + "ukb.dat"

    # Tagger options.
    cfg.TAGGER_HMMFile = lpath + "tagger.dat"
    cfg.TAGGER_ForceSelect = pyfreeling.RETOK

    # Statistical dependency parser & SRL config file.
    cfg.DEP_TreelerFile = lpath + "dep_treeler/dependences.dat"

    # NEC config file. This module will not be loaded.
    cfg.NEC_NECFile = ""

    # Chart parser config file. This module will not be loaded.
    cfg.PARSER_GrammarFile = ""

    # Rule based dependency parser config files. This module will not be loaded.
    cfg.DEP_TxalaFile = ""

    # Coreference resolution config file. This module will not be loaded.
    cfg.COREF_CorefFile = ""

    # create semantic DB module.
    cfg.MACO_SemanticDB = lpath+"semdb.dat"

    return cfg


def fill_invoke():

    ivk = pyfreeling.invoke_options()

    # Level of analysis in input and output
    ivk.InputLevel = pyfreeling.TEXT

    # We can not request higher analysis levels (e.g. coreference) because
    # we didn't load the needed modules.
    # But we can use this option to lowe the analysis level at will during
    # our application execution.
    ivk.OutputLevel = pyfreeling.DEP

    # activate/deactivate morphological analyzer modules
    ivk.MACO_UserMap = False
    ivk.MACO_AffixAnalysis = True
    ivk.MACO_MultiwordsDetection = True
    ivk.MACO_NumbersDetection = True
    ivk.MACO_PunctuationDetection = True
    ivk.MACO_DatesDetection = True
    ivk.MACO_QuantitiesDetection = True
    ivk.MACO_DictionarySearch = True
    ivk.MACO_ProbabilityAssignment = True
    ivk.MACO_CompoundAnalysis = False
    ivk.MACO_NERecognition = True
    ivk.MACO_RetokContractions = False

    ivk.SENSE_WSD_which = pyfreeling.UKB
    ivk.TAGGER_which = pyfreeling.HMM

    # since we only created dep_treeler parser, we can not set the parser to use to another one.
    # If we had loaded both parsers, we could change the used parsed at will with this option
    ivk.DEP_which = pyfreeling.TREELER

    # since we did not load the module, setting this to true would trigger an error.
    # if the module was created, we could activate/deactivate it at will with this option.
    ivk.NEC_NEClassification = False

    return ivk


def extract_lemma_and_sense(word):
    '''
        Extract lemma and sense of word 'w' and store them in 'lem' and 'sens' respectively'.
    '''
    lem = word.get_lemma()
    sens = ""
    if len(word.get_senses()) > 0:
        sens = word.get_senses()[0][0]
    return lem, sens


def get_synsets(list_sent):
    '''
        Retorna todas as palavras que contem seus possíveis synsets.
    '''
    data_temp = []
    for sent in list_sent:
        for word in sent:
            rank = word.get_senses()
            if len(rank) > 0:
                item = {
                    "word": word.get_form(),
                    "possible_synsets": word.get_senses_string(),
                    "selected_synsets": rank[0][0],
                    "probability": str(rank[0][1])
                }
                data_temp.append(item)
    return json.loads(json.dumps(data_temp))


def get_pos_tag(list_sent):
    """
    """
    data_temp = []
    for sent in list_sent:
        for word in sent:
            item = {
                'word': word.get_form(),
                'pos_tag': word.get_tag()
            }
            data_temp.append(item)
    return json.loads(json.dumps(data_temp))


def get_wsd(list_sent):
    """
        WSD - Word Sense Disambiguation

        Usa um anotador de sentido para associar cada palavra a seus possíveis synsets do wordnet, e um 
        desambiguador do sentido da palavra para selecionar o significado correto entre todos os sentidos
        possíveis para cada palavra.

        offset + pos key       =           10285313-n
    """
    data_temp = []

    for sent in list_sent:
        for word in sent:
            rank = word.get_senses()
            if len(rank) > 0:
                item = {
                    "id_token": word.get_position(),
                    "word": word.get_form(),
                    "synset": rank[0][0]
                }
                data_temp.append(item)

    return json.loads(json.dumps(data_temp))


def get_other_analyse(list_sent):
    data_temp = []
    for sent in list_sent:
        for word in sent:
            item = {
                'word': word.get_form(),
                'selected_analysis': word.get_lemma(),
                'tag': word.get_tag()
            }
            data_temp.append(item)
    return json.loads(json.dumps(data_temp))


def get_extrat_svo_triples_srl(list_sent):
    '''
        Analisador Sujeito, Verbo e Objeto (SVO) usando Rotulador de Funções Semanticas (SRL).
        Ele identifica os argumentos de um verbo, independentemente da estrutura síntática de superfície ou do tempo verbal. 
        Tutorial: https://talp-upc.gitbooks.io/freeling-tutorial/content/example05.html
    '''
    for sent in list_sent:
        for pred in sent.get_predicates():
            lsubj = ""
            ssubj = ""
            ldobj = ""
            sdobj = ""

        # for each argument of the predicate.
        for arg in pred:
            # if the argument is A1, store lemma and synset in ldobj, sdobj
            if arg.get_role() == "A1":
                (ldobj, sdobj) = extract_lemma_and_sense(
                    sent[arg.get_position()])
            # if the argument is A0, store lemma and synset in lsubj, subj
            elif arg.get_role() == "A0":
                (lsubj, ssubj) = extract_lemma_and_sense(
                    sent[arg.get_position()])

            # Get tree node corresponding to the word marked as argument head
            head = sent.get_dep_tree().get_node_by_pos(arg.get_position())

            # check if the node has dependency is "by" in passive structure
            if lsubj == "by" and head.get_label == "LGS":
                # get first (and only) child, and use it as actual subject
                head = head.get_nth_child(0)
                (lsubj, ssubj) = extract_lemma_and_sense(head.get_word())

        # if the predicate had both A0 and A1, we found a complete SVO triple. Let's output it.
        if lsubj != "" and ldobj != "":
            (lpred, spred) = extract_lemma_and_sense(sent[pred.get_position()])

            item = {
                "svo":{
                    "pred":{
                        "lemma": lpred,
                        "synset": spred
                    },
                    "subject":{
                        "lemma": lsubj,
                        "synset": ssubj
                    },
                    "dobject":{
                        "lemma": ldobj,
                        "synset": sdobj
                    } 
                }
            }

            #print ("SVO : (pred:   ", lpred, "[" + spred + "]")
            #print ("       subject:", lsubj, "[" + ssubj + "]")
            #print ("       dobject:", ldobj, "[" + sdobj + "]")
            #print ("      )")

            return json.loads(json.dumps(item))
            
            


def get_ontologia_class(list_sent):
    '''
        Description: Navegação no WordNet. Procurar informações semanticas. Verifica se uma palavra 
                    pentence a class animal na ontologia SUMO.
        Tutorial:
            https://talp-upc.gitbooks.io/freeling-tutorial/content/semantics.html
            https://talp-upc.gitbooks.io/freeling-tutorial/content/example06.html
            https://talp-upc.gitbooks.io/freeling-tutorial/content/example07.html
            http://www.adampease.org/OP/
    '''

    # create semantic DB module
    sdb = pyfreeling.semanticDB(lpath+"semdb.dat")

    for sent in list_sent:
        for word in sent:
            print(word.get_form(), word.get_lemma(), word.get_tag())

            if len(word.get_senses()) > 0:
                syns = word.get_senses()[0][0]
                print(" "+syns)
                # move up to parent until a synset with SUMO == "Animal" is found.
                is_top = False
                is_animal = False

                while not is_top and not is_animal:
                    si = sdb.get_sense_info(syns)
                    is_animal = (si.sumo == "Animal=")

                    # climb one more node if possible
                    if len(si.parents) > 0:
                        syns = si.parents[0]
                    else:
                        is_top = True

                if is_animal:
                    print("    *** SUMO 'Animal='")

            # next word
            print("")

        # sentence separator
        print("")


'''
    MAIN
'''
# set locale to an UTF8 compatible locale
pyfreeling.util_init_locale("default")

# get requested language from arg1, or English if not provided
LANG = config.LANG['freeling']
if len(sys.argv) > 1:
    LANG = sys.argv[1]

# get installation path to use from arg2, or use /usr/local if not provided
ipath = "/usr/local"
if len(sys.argv) > 2:
    ipath = sys.argv[2]

# path to language data.
lpath = ipath + "/share/freeling/" + LANG + "/"

# set config options (which modules to create, with which configuration)
cfg = fill_config(LANG, ipath)

# create analyzer.
anlz = pyfreeling.analyzer(cfg)

# set invoke options (which modules to use. Can be changed in run time)
ivk = fill_invoke()

# load invoke options into analyzer.
anlz.set_current_invoke_options(ivk)


def analyze(sent):
    """
    """
    #ls = anlz.analyze_as_sentences(str(text),True)
    return anlz.analyze(sent, True)
