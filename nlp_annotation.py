# -*- coding: utf-8 -*-

"""
    Software: nlp Deep
    Description:
    Date: 22/07/2018
"""
import os
import ast
import tool
import json
import pathlib
import tempfile
import config.settings as config

import numpy as np
import xmlformatter

import util.log as log
import util.loaddata as ld
import util.pprocessing as pp
import util.telegram as telegram

import modules.myxmlformatter as myxmlform
# import modules.botnot.toast as toast

import tool.MyFreeLing as freeling
import tool.MySEMAFOR as semafor

from tqdm import tqdm
from time import sleep
from datetime import datetime
from multiprocessing import Process


basedir = os.path.abspath(os.path.dirname(__file__))



def annotation(options_selected, name_new_file_out):

    lexsedi = tool.LexSeDi()


    # pln
    scnlp = tool.MyStanfordCoreNLP()
    spacy = tool.MySpaCy()
    nltk = tool.MyNLTK()
    cogcomp = tool.MyCogComp()

    # dict
    # verbnet = tool.MyVerbNet()
    wordnet = tool.MyWordNet()
    wnd = tool.MyWordNetDomains()
    xwnd = tool.MyXWordNetDomains()
    gazetteer = tool.MyGate()
    asumo = tool.MyAdimenSUMO()

    # wsd
    supwsd = tool.MySupWSD()
    
    if config.TELEGRAM['SEND_MSG']: telegram.send_message(config.SIMB[4])

    """
        1 - Raw Text.
    """
    # 1.1 Load dataset (one sentence below the other).
    #dataset = ld.load_data(config.PATH['path_file_in'] + '/' + options_selected['dataset']) 


    """
        2 - PrepProcessing
    """
    #if options_selected['sentences_splits']:
        #sent_temp = []
        #json_data = scnlp.annotate_ssplit(dataset)
        

    
    # word tokenize all sentences, save in file temp.
    if True:
        # open file temp dataset armazening sent tokenization.
        temp_dataset_sent_tokens = open(config.PATH['path_dir_temp']+'/'+config.FILE['temp_tokens'], 'w')

        telegram.send_message('Loading dataset..')
        #toast.send_notification('processing', 'Deep', 'Loading dataset..')
        log.logging.info('Loading dataset..')

        sents = ld.load_data(config.PATH['path_file_in'] + '/' + options_selected['dataset'])

        #toast.send_notification('processing', 'Deep', 'Tokenization default dataset..')
        log.logging.info(' ├── Tokenization default dataset..')

        for sent in sents:
            s = ' '.join(scnlp.word_tokenize(sent))
            temp_dataset_sent_tokens.write(s + '\n')

        temp_dataset_sent_tokens.close()

    # POS-PROCESSING
    telegram.send_message('Loading dataset tokenized..')
    #toast.send_notification('processing', 'Deep', 'Loading dataset tokenized..')
    log.logging.info(' ├── Loading dataset tokenized..')

    # carrega todas as sentenças uma abaixo da outra.
    sentences = ld.load_data(config.PATH['path_dir_temp']+'/'+config.FILE['temp_tokens'])

    telegram.send_message('Processing sentences..')
    #toast.send_notification('processing', 'Deep', 'Processing sentences..')
    log.logging.info(' ├── Processing data..')

    if config.CONFIG['create_new_file_out']:
        data_temp = open(config.PATH['path_file_out'] + name_new_file_out, 'w')
    else:
        data_temp = open(config.PATH['path_file_out'] + config.FILE['output'], 'w')

    # BEGIN XML.
    # data_temp.write('<?xml version="1.0" encoding="UTF-8"?>')
    # data_temp.write('<?xml-stylesheet href="LexSeDi-to-HTML.xsl" type="text/xsl" datetime="{datetime}"?>'.format(datetime=datetime.now()))
    data_temp.write('<document filename="{filename}" type="text/xsl" version="1.0" encoding="UTF-8">'.format( filename=name_new_file_out ) )
    data_temp.write( '<sentences cont="{sentences}">'.format(sentences=len(sentences)) )

    s_id = 0
    t_id = 0
    ck_id = 0

    # Loop processing.
    for sentence in tqdm(sentences):
        s_id += 1

        data = scnlp.annotate_main(sentence)

        # sentences.
        data_temp.write('<sentence s_id="s_{s_id}" text="{sentence}" tokens="{tokens}" >'.format(s_id=s_id, sentence=sentence, tokens=len(scnlp.word_tokenize(sentence)) ))

        # loop sent.
        for sent in data['sentences']:

            '''
                Analysis: Lexical, Sintactic.
            '''
            data_temp.write('<lexical_syntactic s_id="s_{s_id}" >'.format(s_id=s_id))



            '''
                Analysi: Tokens
                Tool: Stanford CoreNLP, SpaCy, Proper.
                Description: Some properties of tokens are only displayed when they exist (ner_stanford, ner_spacy, true_case, gazetteer).
            '''
            data_temp.write('<tokens>')

            for token in sent['tokens']:

                _ner_stanford = 'ner="'+token['ner']+'"' if token['ner'] != "O" and options_selected['ner_corenlp'] else ""

                ner_spacy = str(spacy.get_ner(token['word']))
                _ner_spacy = 'ner_spacy="'+ner_spacy+'"' if ner_spacy != token['word'] and options_selected['ner_spacy'] else ""
                
                _true_case = 'true_case="'+token['truecase']+'"' if token['truecase'] != 'O' and options_selected['true_case'] else ""

                gt = gazetteer.get_category(token['word'])
                _gazetteer = 'gazetteer="'+gt+'"' if gt != 'null' and options_selected['gazzetter'] else ""

                data_temp.write('<token s_id="s_{s_id}" t_id="t_{t_id}" word="{word}" lemma="{lemma}" stem="{stem}" length="{length}" pos="{pos}" {ner_stanford} {ner_spacy} shape="{shape}" morph_type="{morph_type}" {truecase} {gazetteer}/>'.format(
                    s_id=s_id, t_id=token['index'], word=token['word'], lemma=token['lemma'], stem=nltk.stemming_porter(token['word']), length=len(token['word']), pos=token['pos'], ner_stanford=_ner_stanford, ner_spacy=_ner_spacy, shape=spacy.shape(token['word']), morph_type=lexsedi.get_morph_type(token['word']), truecase=_true_case, gazetteer=_gazetteer ))
            
            data_temp.write('</tokens>')


            
            '''
                Analysi: Shallow Parse (chunkings).
                Tool: CogComp (Online)

                TODO: Adicionar essa função Offline
            '''
            if options_selected['shallow_parsing_cogcomp']:

                json_data_csp = cogcomp.server_web(sentence, 'SHALLOW_PARSE')

                for data_csp in json_data_csp['views']:
                    if data_csp['viewName'] == "SHALLOW_PARSE":
                        data_temp.write('<chunkings s_id="s_{s_id}">'.format(s_id=s_id))
                        ck_id = 0

                        for constituent in data_csp['viewData'][0]['constituents']:
                            if constituent['label'] != '':
                                ck_id += 1

                                data_temp.write('<chunk ck_id="{ck_id}" type="{type}">'.format(ck_id=ck_id, type=constituent['label']))
                                t_id_csp = 0
                                core_csp = 0

                                # control value t_id.
                                t_id = constituent['start']

                                for token_csp in json_data_csp['tokens'][constituent['start']:constituent['end']]:
                                    t_id_csp += 1
                                    core_csp += 1

                                    # verifica se é o último para adicionar o core_type.
                                    if core_csp != len(json_data_csp['tokens'][constituent['start']:constituent['end']]):
                                        data_temp.write('<token t_id="t_{t_id}" string="{token}"/>'.format(t_id=t_id, token=token_csp))
                                    else:
                                        data_temp.write('<token t_id="t_{t_id}" string="{token}" core{type}="true"/>'.format(t_id=t_id, token=token_csp, type=constituent['label']))

                                    t_id += 1

                                data_temp.write('</chunk>')
                        data_temp.write('</chunkings>')


            if options_selected['dependencies_parsing']:
                '''
                    Analysi: dependencies constituency parse.
                    Tool: Stanford CoreNLP 
                '''
                data_temp.write('<constituency_parse>')
                data_temp.write('<input text="{parse}" />'.format(parse=pp.remove_extra_spacing(sent['parse'].replace('\n', '').replace('\r', ''))))
                data_temp.write('</constituency_parse>')



                '''
                    Analysi: Dependencies
                    Tool: Stanford CoreNLP (offline)
                '''
                data_temp.write('<dependencies>')

                # stanford dependencies basic, enhanced and enhanced plus plus.
                all_dep_view = ['basicDependencies', 'enhancedDependencies', 'enhancedPlusPlusDependencies']
                format_input = []

                # input
                for dep_basic in sent['basicDependencies']:
                    aux = str(dep_basic['dep']) + '(' + str(dep_basic['governorGloss']) + '-' + str(
                        dep_basic['governor']) + ',' + str(dep_basic['dependentGloss']) + '-' + str(dep_basic['dependent']) + ')'
                    format_input.append(aux)

                data_temp.write(
                    '<input text="{text}" />'.format(text="".join(format_input).replace(')', ')&#xA;')))

                for dep_view in all_dep_view:
                    data_temp.write('<dependencies_stanford type="{type}">'.format(type=dep_view))

                    # pair.
                    for dep_basic in sent[dep_view]:
                        data_temp.write('<pair rel="{rel}">'.format(rel=dep_basic['dep']))
                        data_temp.write('<arg1 id="t_{id}" seq="{id}" string="{governorGloss}"/>'.format(id=dep_basic['governor'], governorGloss=dep_basic['governorGloss']))
                        data_temp.write('<arg2 id="t_{id}" seq="{id}" string="{dependentGloss}"/>'.format(id=dep_basic['dependent'], dependentGloss=dep_basic['dependentGloss']))
                        data_temp.write('</pair>')

                    data_temp.write('</dependencies_stanford>')

                data_temp.write('</dependencies>')

            data_temp.write('</lexical_syntactic>')




            '''
                Semantics Analysis.
            '''
            if options_selected['wsd_freeling'] or options_selected['wsd_supwsd'] or options_selected['srl_cogcomp'] or options_selected['svo_srl_freeling'] or options_selected['semafor']:
                data_temp.write('<semantics_analysis>')

            if options_selected['wsd_freeling'] or options_selected['wsd_supwsd']:
                data_temp.write('<wsd>')



            '''
                Analyze: WSD
                Tool: FreeLing (offline)
            '''
            if options_selected['wsd_freeling']:
                try:
                    json_data_fl = freeling.get_wsd(freeling.analyze(sentence))

                    if json_data_fl != None:
                        data_temp.write('<freeling s_id="s_{s_id}" version="4.1">'.format(s_id=s_id))

                        for data_fl in json_data_fl:
                            _synset = wordnet.get_offset_pos_key(data_fl['synset'])

                            # ======================== Info WSD ========================
                            category_wnd = wnd.get_category(data_fl['synset'])
                            _category_wnd = 'wnd="'+category_wnd+'"' if category_wnd != "null" else ""

                            category_asumo = asumo.get_category(data_fl['synset'])
                            _category_asumo = 'asumo="'+category_asumo+'"' if category_asumo != "null" else ""

                            data_temp.write('<token id_t="t_{id_t}" word="{word}" sense="{sense}" {wnd} {asumo}>'.format(
                                id_t=data_fl['id_token'], word=data_fl['word'], sense=data_fl['synset'], wnd=_category_wnd, asumo=_category_asumo ))

                            # ======================== WordNet ========================
                            data_temp.write('<wordnet version="3.0">')

                            synsets = wordnet.get_synsets(data_fl['word'])
                            data_temp.write('<synsets data="{synsets}"/>'.format(synsets=synsets))

                            data_temp.write('<definition data="{definition}"/>'.format(definition=wordnet.get_definition(_synset)))

                            _synonyms = wordnet.get_synonyms(data_fl['word'], 5)
                            if _synonyms != None:
                                data_temp.write('<synonyms words="{words}"/>'.format(words=_synonyms))

                            _antonyms = wordnet.get_antonyms(data_fl['word'])
                            if _antonyms != '':
                                data_temp.write('<antonyms words="{words}"/>'.format(words=_antonyms))

                            _hypernyms = wordnet.get_hypernym_paths(_synset, 0)
                            if _hypernyms != '':
                                data_temp.write('<hypernyms words="{words}"/>'.format(words=_hypernyms))

                            _hyponyms = wordnet.get_hyponyms(_synset, 5)
                            if _hyponyms != '':
                                data_temp.write('<hyponyms words="{words}"/>'.format(words=_hyponyms))

                            phrases = wordnet.get_examples(_synset)
                            if phrases > '0':
                                data_temp.write('<examples quantity="{quantity}">'.format(quantity=len(phrases)))
                                for phrase in phrases:
                                    data_temp.write('<example phrase="{phrase}"/>'.format(phrase=phrase.replace('"', '').replace("'", '')))
                                data_temp.write('</examples>')

                            member_holonyms = wordnet.get_member_holonyms(_synset)
                            if member_holonyms != '[]':
                                data_temp.write('<member_holonyms data="{member_holonyms}" />'.format(member_holonyms=member_holonyms))

                            entailments = wordnet.get_entailments(_synset)
                            if entailments != '':
                                data_temp.write('<entailments data="{entailments}"/>'.format(entailments=entailments))

                            data_temp.write('</wordnet>')  # end.

                            # ======================== VerbNet ========================

                            '''
                            # split synset.
                            synset, pos = data_fl['synset'].split('-') # 00056930-v

                            # check is verb.
                            if pos == 'v':
                                data_temp.write('<verbnet version="3.2b">')

                                classids = verbnet.get_classids_to_lemma(_synset) 

                                if classids:
                                    data_temp.write(
                                        '<classids data="{classids}"/>'.format(classids=classids))

                                    lemmas = verbnet.get_lemmas(classids)
                                    if lemmas:
                                        data_temp.write(
                                            '<lemmas data="{lemmas}"/>'.format(lemmas=lemmas))

                                    subclasses = verbnet.get_subclasses(classids)
                                    if subclasses:
                                        data_temp.write(
                                            '<subclasses data="{subclasses}"/>'.format(subclasses=subclasses))

                                    themroles = verbnet.get_themroles(classids)
                                    if themroles:
                                        data_temp.write(
                                            '<themroles data="{themroles}"/>'.format(themroles=themroles))

                                    vnclass = verbnet.get_vnclass(classids)
                                    if vnclass:
                                        data_temp.write(
                                            '<vnclass data="{vnclass}"/>'.format(vnclass=vnclass))

                                data_temp.write('</verbnet>')
                                '''

                            data_temp.write('</token>')

                        data_temp.write('</freeling>')
                except Exception as err:
                    log.logging.error("FreeLing WSD Exception occurred \n Sentence: "+sentence, exc_info=True)



            '''
                Analyze: WSD
                Tool: SupWSD (online)
            '''
            if options_selected['wsd_supwsd']:
                try:
                    json_data_supwsd = supwsd.disambiguate(sentence)
                    data_temp.write('<supwsd s_id="s_{s_id}">'.format(s_id=s_id))
                    t_id = 0

                    for data_supwsd in json_data_supwsd:

                        # control id tokens, check word some tokens.
                        quant_token_join = data_supwsd['word'].count('_')
                        
                        # to = 5
                        if quant_token_join > 0:
                            #print 'quant_token_join', quant_token_join
                            quant_token_join = t_id + quant_token_join + 2 # corect err cont [tokens, delimitador].
                        else:
                            t_id += 1

                        # check token null.
                        if data_supwsd['sense'] != "U":
                            _synset = wordnet.get_lemma_key(data_supwsd['sense'])

                            # ======================== Info WSD ========================
                            if quant_token_join > 0:
                                data_temp.write('<token t_id="{t_ids}" chunk="{word}" lemma="{lemma}" pos="{pos}" sense_wn="{sense}" synset_wn="{synset_wn}" xwnd="{xwnd}">'.format(
                                    t_ids=[ n for n in range(t_id+1, quant_token_join) ], word=data_supwsd['word'], lemma=data_supwsd['lemma'], pos=data_supwsd['pos'], sense=data_supwsd['sense'], synset_wn=_synset, xwnd=xwnd.get_domains(_synset)))
                                    
                                sub = quant_token_join - (t_id+1) # corect errr.
                                t_id = t_id + sub #update t_id
                            else:
                                data_temp.write('<token t_id="t_{t_id}" word="{word}" lemma="{lemma}" pos="{pos}" sense_wn="{sense}" synset_wn="{synset_wn}" xwnd="{xwnd}">'.format(
                                    t_id=t_id, word=data_supwsd['word'], lemma=data_supwsd['lemma'], pos=data_supwsd['pos'], sense=data_supwsd['sense'], synset_wn=_synset, xwnd=xwnd.get_domains(_synset)))

                            # ======================== Begin WordNet ========================
                            try:
                                data_temp.write('<wordnet version="3.0">')

                                _definition = wordnet.get_definition(_synset)
                                if _definition != None:
                                    data_temp.write('<definition data="{definition}"/>'.format(definition=_definition))

                                _synonyms = wordnet.get_synonyms(data_supwsd['word'], 5)
                                if _synonyms != None:
                                    data_temp.write('<synonyms words="{synonyms}"/>'.format(synonyms=_synonyms))

                                _antonyms = wordnet.get_antonyms(data_supwsd['word'])
                                if _antonyms != '':
                                    data_temp.write('<antonyms words="{antonyms}"/>'.format(antonyms=_antonyms))

                                _hypernyms = wordnet.get_hypernym_paths(_synset, 0)
                                if _hypernyms != '':
                                    data_temp.write('<hypernyms data="{hypernyms}"/>'.format(hypernyms=_hypernyms))

                                _hyponyms = wordnet.get_hyponyms(_synset, 5)
                                if _hyponyms != '':
                                    data_temp.write('<hyponyms data="{hyponyms}"/>'.format(hyponyms=_hyponyms))

                                _member_holonyms = wordnet.get_entailments(_synset)
                                if _member_holonyms != '': 
                                    data_temp.write('<member_holonyms data="{member_holonyms}"/>'.format(member_holonyms=_member_holonyms))

                                examples_phrases = wordnet.get_examples(wordnet.get_lemma_key(data_supwsd['sense']))

                                if examples_phrases > '0':
                                    data_temp.write('<examples quantity="{quantity}">'.format(quantity=len(examples_phrases)))
                                    cont_phrase = 0
                                    for phrase in examples_phrases:
                                        cont_phrase += 1
                                        data_temp.write('<example phrase_{cont}="{phrase}"/>'.format(cont=cont_phrase, phrase=phrase.replace("'", '').replace('"', '') ))
                                    cont_phrase = 0
                                    data_temp.write('</examples>')

                                data_temp.write('</wordnet>')

                            except Exception as err:
                                log.logging.error("SupWSD (WordNet) Exception occurred \n Sentence: "+sentence, exc_info=True)

                            # ======================== Begin VerbNet ========================
                            '''
                            try:
                                # check token is verb.
                                if str(data_supwsd['pos']) == 'SupPOS.VERB':

                                    # get sense and format.
                                    _classid = verbnet.get_classid_to_wordnetid(data_supwsd['sense'].replace("::", ""))

                                    # check is exist classid.
                                    if _classid:
                                        data_temp.write('<verbnet version="3.2b">')

                                        if _classid:
                                            data_temp.write('<classid data="{classid}"/>'.format(classid=_classid))

                                        _classids = verbnet.get_classids_to_classid(_classid)
                                        if _classids:
                                            data_temp.write('<classids data="{classids}"/>'.format(classids=_classids))

                                        _sub_class = verbnet.get_subclasses(_classid)
                                        if _sub_class:
                                            data_temp.write('<sub_class data="{subclass}"/>'.format(subclass=_sub_class))

                                        _themroles = verbnet.get_themroles(
                                            _classid)
                                        if _themroles:
                                            data_temp.write('<themroles data="{themroles}"/>'.format(themroles=_themroles))

                                        _members = verbnet.get_members(
                                            _classid)
                                        if _members:
                                            data_temp.write('<members data="{members}"/>'.format(members=_members))

                                        data_temp.write('</verbnet>')

                            except Exception as err:
                                log.logging.error("SupWSD (VerbNet) Exception occurred", exc_info=True)
                            '''

                            data_temp.write('</token>')
                    data_temp.write('</supwsd>')

                except Exception as err:
                    log.logging.error("SupWSD Exception occurred \n Sentence: "+sentence, exc_info=True)

            if options_selected['wsd_freeling'] or options_selected['wsd_supwsd']:
                data_temp.write('</wsd>')



            '''
                Analyze: Frame Semantic Parse
                Tool: SEMAFOR (offline)
            '''
            if options_selected['semafor']:
                try:
                    json_datasemafor = semafor.get_parse(sentence)

                    if json_datasemafor:
                        data_temp.write('<frame_semantic_parse s_id="s_{s_id}" semafor="3.0 alfa" framenet="1.5">'.format(s_id=s_id))
                        data_temp.write('<frames>')
                        
                        for frame in json_datasemafor['frames']:
                            data_temp.write('<frame>')
                            
                            if frame['target']:
                                quant_element = frame['target']['spans'][0]['end'] - frame['target']['spans'][0]['start']
                                
                                if quant_element == 1:
                                    id_t = frame['target']['spans'][0]['start'] + 1
                                    data_temp.write('<target id_t="{id_t}" token="{token}" name="{name}"/>'.format(id_t=id_t, token=frame['target']['spans'][0]['text'], name=frame['target']['name']))
                                else:
                                    data_temp.write('<target id_t="{id_t}" chunk="{token}" name="{name}"/>'.format(id_t=[ t_id+1 for t_id in range(frame['target']['spans'][0]['start'], frame['target']['spans'][0]['end'] ) ], token=frame['target']['spans'][0]['text'], name=frame['target']['name']))

                            if frame['annotationSets'][0]['frameElements']:
                                data_temp.write('<frame_elements>')

                                for element in frame['annotationSets'][0]['frameElements']:
                                    quant_element = element['spans'][0]['end'] - element['spans'][0]['start']

                                    if quant_element == 1:
                                        id_t = element['spans'][0]['start'] + 1
                                        data_temp.write('<element id_t="t_{id_t}" token="{text}" name="{name}"/>'.format(id_t=id_t, text=element['spans'][0]['text'], name=element['name']))
                                    else:
                                        data_temp.write('<element id_t="{id_t}" chunk="{text}" name="{name}"/>'.format(id_t=[ t_id+1 for t_id in range(element['spans'][0]['start'], element['spans'][0]['end'] ) ], text=element['spans'][0]['text'], name=element['name']))
                                
                                data_temp.write('</frame_elements>')

                            data_temp.write('</frame>')

                        data_temp.write('</frames>')
                        data_temp.write('</frame_semantic_parse>')

                except Exception as err:
                    log.logging.error("SEMAFOR Exception occurred \n Sentence: {sentence}".format(sentence), exc_info=True)



            if options_selected['srl_cogcomp'] or options_selected['svo_srl_freeling']:
                data_temp.write('<srl>')


            
            '''
                Analyse: SRL
                Tool: CogComp (online)
            '''
            if options_selected['srl_cogcomp']:

                views = ['SRL_VERB', 'SRL_PREP', 'SRL_NOM']
                data_temp.write('<cogcomp s_id="s_{s_id}">'.format(s_id=s_id))

                for view in views:
                    try:
                        json_data_srl = cogcomp.server_web(sentence, view)

                        for data_srl in json_data_srl['views']:
                            if data_srl['viewName'] == view:
                                if data_srl['viewData'][0]['constituents']:
                                    data_temp.write('<'+view.lower()+'>')
                                    
                                    data_temp.write('<constituents>')
                                    for data_srl in json_data_srl['views']:
                                        if data_srl['viewName'] == view:
                                            cont_constituent = 0
                                            for constituent in data_srl['viewData'][0]['constituents']:
                                                if len(constituent) > 4:
                                                    data_temp.write('<arg constituent="{constituent}" label="{label}">'.format(constituent=cont_constituent, label=constituent['label']))
                                                    data_temp.write('<tokens>')
                                                    for token in json_data_srl['tokens'][constituent['start']:constituent['end']]:
                                                        data_temp.write('<token t_id="t_{t_id}" token="{token}"/>'.format(t_id=constituent['start'], token=token))
                                                    data_temp.write('</tokens>')
                                                    data_temp.write('<properties SenseNumber="{SenseNumber}" predicate="{predicate}"/>'.format(
                                                        SenseNumber=constituent['properties']['SenseNumber'], predicate=constituent['properties']['predicate']))
                                                    data_temp.write('</arg>')
                                                else:
                                                    if constituent['label'] != "":
                                                        data_temp.write('<arg constituent="{constituent}" label="{label}">'.format(constituent=cont_constituent, label=constituent['label']))
                                                        data_temp.write('<tokens>')

                                                        t_id = constituent['start'] # control token id .

                                                        for token in json_data_srl['tokens'][constituent['start']:constituent['end']]:
                                                            data_temp.write('<token t_id="t_{t_id}" token="{token}"/>'.format(t_id=t_id, token=token))
                                                            t_id += 1
                                                        
                                                        data_temp.write('</tokens>')
                                                        data_temp.write('</arg>')
                                                cont_constituent += 1
                                    data_temp.write('</constituents>')

                                    # check is relations
                                    for data_srl in json_data_srl['views']:
                                        if data_srl['viewName'] == view:
                                            try:
                                                if data_srl['viewData'][0]['relations']:
                                                    data_temp.write('<relations>')
                                                    for data_srl in json_data_srl['views']:
                                                        if data_srl['viewName'] == view:
                                                            for relations in data_srl['viewData'][0]['relations']:
                                                                data_temp.write('<relation srcConstituent="{srcConstituent}" role="{relationName}" targetConstituent="{targetConstituent}"/>'.format(
                                                                    relationName=relations['relationName'], srcConstituent=relations['srcConstituent'], targetConstituent=relations['targetConstituent']))
                                                    data_temp.write('</relations>')
                                            except Exception as err:
                                                log.logging.error("Cogcomp "+view+"  Exception occurred: There are no relationships in sentence. \nSentence: "+sentence, exc_info=True)

                                    data_temp.write('</'+view.lower()+'>')

                    except Exception as err:
                        log.logging.error("Cogcomp "+view+"  Exception occurred \n Sentence: "+sentence, exc_info=True)

                data_temp.write('</cogcomp>')



            '''
                Analyse: SVO SRL
                Tool: FreeLing (offline)
            '''
            if options_selected['svo_srl_freeling']:
                try:
                    json_data_svo = freeling.get_extrat_svo_triples_srl(freeling.analyze(sentence))
                    
                    if json_data_svo != None:
                        data_temp.write('<svo_srl s_id="s_{s_id}" freeling="4.1">'.format(s_id=s_id))

                        for data_svo in json_data_svo['svo']:
                            data_temp.write('<'+data_svo+' lemma="{lemma}" synset_wn="{synset_wn}" />'.format(lemma=json_data_svo['svo'][data_svo]['lemma'], synset_wn=json_data_svo['svo'][data_svo]['synset']))
                        
                        data_temp.write('</svo_srl>')

                except Exception as err:
                    log.logging.error("FreeLing SVO SRL Exception occurred \n Sentence: "+sentence, exc_info=True)



            if options_selected['srl_cogcomp'] or options_selected['svo_srl_freeling']:
                data_temp.write('</srl>')

            if options_selected['wsd_freeling'] or options_selected['wsd_supwsd'] or options_selected['srl_cogcomp'] or options_selected['svo_srl_freeling'] or options_selected['semafor']:
                data_temp.write('</semantics_analysis>')

        data_temp.write('</sentence>')

    data_temp.write('</sentences>')

    data_temp.write('</document>')

    data_temp.close()

    # Success
    telegram.send_message('File out generated in: {path}/dataset/output/{fileout}'.format(path=pathlib.Path.cwd(), fileout=name_new_file_out))
    #toast.send_notification('lexsedi', 'Deep', 'End: File out generated in: {path}/dataset/output/{fileout}'.format(path=pathlib.Path.cwd(), fileout=name_new_file_out))



def run(options_selected):
    try:
        dataset_name, ext = options_selected['dataset'].split('.')
        
        # name_dataset#data#time
        name_new_file_out = str(dataset_name) + '#' + str(datetime.now()).replace(' ', '#') + '.xml'

        process = Process( target=annotation( json.loads( json.dumps(options_selected) ), name_new_file_out ) )
        process.start()
        process.join()
        return myxmlform.string_xml_formatter( open(config.PATH['path_file_out']+name_new_file_out, 'r').read() )
    except Exception as err:
        telegram.send_message('Exception occurred {error}'.format(error=err))
        #toast.send_notification('error', 'Exception occurred', str(err))
        log.logging.error("Exception occurred:", exc_info=True)
        return err



if __name__ == "__main__":
    with open(basedir+'/config/analysis.txt') as file:
        options_selected = ast.literal_eval(file.read())
        run( options_selected )