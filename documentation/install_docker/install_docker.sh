#!/bin/sh

# Install Docker in Computer
# Tutorial base <https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-16-04-pt>

# Update
apt-get update

# Add the official GPG key from the Docker repository to the system
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

# Add the Docker repository to APT sources
apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'

# Update
apt-get update

# Make sure that you are installing from the Docker repository instead of the default Ubuntu repository
apt-cache policy docker-engine

# Finally, install Docker
apt-get install -y docker-engine

# Verify that it is running
systemctl status docker