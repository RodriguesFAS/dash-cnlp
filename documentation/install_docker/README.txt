# Stap 1

    ## To install docker on the computer run the script: 

        $ ./install_docker.sh

# Stap 2 

    ## To install the Deep NLP DashBoard using docker, run the following comand:
    ## path: /home/documentations/install_docker

        ### To build the image.
        $ docker build -t rodriguesfas/deep_nlp_dashboard:1.0.0 .

## Stap 3 

    ### Run Docker Image Deep NLP DashBoard.
    $ docker images -a

    ### Copy the image_id of rodrigues/deep_nlp_dashboard
    $ docker run -i -t -p 5000:5000 image_id /bin/bash
    $ python run_server.py

## Stap

    http://127.0.0.1:5000/