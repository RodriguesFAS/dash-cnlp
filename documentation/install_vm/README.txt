1 - Install VM ORACLE on the computer run the script:

    $ ./install_virtual_box_oracle.sh

2 - Download VDI Deep NLP DashBoard
## https://drive.google.com/open?id=11xIF2uCgTNxFI4sesHOfomV3H39g5Te2
## Unzip the downloaded file

3 - Install VDI in VM ORACLE

3.1 Open Virtualbox

3.2 Selected option New

### Name: Deepin
### Type: Linux
### Version: Other Linux (64bits)

3.3 Define size RAM Configuration >= 12GB (12288)

3.4 Option [3] - Use an Existing Virtual Hard Disk

### Select the VDI file you downloaded.
### click in Create

4 - VM Config Extra
## CPU min 2
## Rede to enable
## bidirectional file transfer (optional)

5 - Start VM created

## VM Login
### Pass: 21072012

6 - Inside the VM in the desktop has a README file with instructions to run the application Deep NLP DasBoard