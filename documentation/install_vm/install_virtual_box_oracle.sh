#!/bin/sh

# Install software Virtual Box Oracle

## Let’s import the Oracle public key to your system signed the Debian packages using the following commands.
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -

## Now, you need to add Oracle VirtualBox PPA to Ubuntu system. 
## You can do this by running the below command on your system.
add-apt-repository "deb http://download.virtualbox.org/virtualbox/debian `lsb_release -cs` contrib"

## Install Oracle VirtualBox
apt-get update
apt-get install virtualbox-5.2

## Launch VirtualBox
virtualbox