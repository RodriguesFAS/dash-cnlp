"""
    File: Join Category Gazetteer Gate
    Description: This script reads all Gazetteer file and generates a single file with all information.
    Date: 28/09/2018
"""

import os

path_xwnd = '/home/rodriguesfas/Mestrado/workspace/lexsedi/resources/dict/gazetteer-gate-8.5.1/'
path_xwnd_generated = '/home/rodriguesfas/Mestrado/workspace/lexsedi/resources/dict/gazetteer-gate-8.5.1/gazetteer-gate-8.5.1.csv'


def main(_path):
    data_temp = open(path_xwnd_generated, 'w')
    for file in os.listdir(_path):
        if file.endswith('.lst'):
            category, ext = file.split('.', 1)
            data = open(_path + file).read()
            for line in data.split('\n'):
                if line != '':
                    line = line.replace(' ', '_').replace(",", "")
                    data_temp.write(line+', '+category+'\n')
    data_temp.close()


if __name__ == '__main__':
    main(path_xwnd)
    print 'File generated!'
