"""
    File: Join Category XWND
    Description: This script reads all extended Wordains Domains file and generates a single file with all information.
    Date: 26/09/2018
"""

import os

path_xwnd = '/home/rodriguesfas/Tool-NLP/eXtendedWordNetDomains/xwnd-30g/xwnd-30g/'
path_xwnd_generated = '/home/rodriguesfas/Mestrado/workspace/lexsedi/resources/dict/xwnd-30g/xwnd-30g.csv'


def main(_path):
    data_temp = open(path_xwnd_generated, 'w')
    for file in os.listdir(_path):
        if file.endswith('.ppv'):
            category, ext = file.split('.')
            data = open(_path + file).read()
            for line in data.split('\n'):
                if line != '':
                    synset, weight = line.split('	')
                    data_temp.write(synset+','+weight+','+category+'\n')
    data_temp.close()


if __name__ == '__main__':
    main(path_xwnd)
    print 'File generated!'
