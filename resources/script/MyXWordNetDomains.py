"""
    Class: MyXWordNetDomains
    Description:
    Data: 26/09/2018
"""

import config


class MyXWordNetDomains(object):
    """
    """

    xwnd_dict = {}

    def __init__(self):
        self.data = self.load_data()

    def load_data(self):
        """
            @return
        """
        file = open(config.FILE['xwnd']).read()
        for line in file.split('\n'):
            if line != '':
                synset, weight, category = line.split(',')
                aux = weight, category
                self.xwnd_dict.setdefault(synset, []).append(aux)

    def get_category(self, _synset):
        """
            @param _synset

            @return
        """
        if self.xwnd_dict.get(_synset):
            p_resul = self.xwnd_dict.get(_synset)
            
            _max = 0
            _cat = ''
            
            # select higher probability.
            for p in p_resul:
                if p[0] > _max:
                    _max = p[0]
                    _cat =  p[1]
            
            # check if there are other categories with the same probability.
            _list = []

            for p in p_resul:
                if p[0] == _max:
                    _list.append(p[1])

            return ' '.join(_list).strip()
        else:
            return 'null'
