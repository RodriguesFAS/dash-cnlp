# Mapping between Adimen-SUMO v2.6 and WordNet v3.0 (Adimen-SUMO)

Adimen-SUMO is an off-the-shelf first-order ontology that has been obtained by reengineering out of the 88% of SUMO (Suggested Upper Merged Ontology). Adimen-SUMO can be used appropriately by FO theorem provers (like E-Prover or Vampire) for formal reasoning.

The contribution of Adimen-SUMO to the area of ontological formal reasoning is threefold. Firstly, we translated SUMO from its original format into the standard first order language. Secondly, we used first-order theorem provers as inference engines for debugging the ontology. Thus, we detected and repaired several significant problems with the axiomatization of the SUMO ontology. Problems we encountered include incorrectly defined axioms, redundancies, non-desirable properties, and axioms that do not produce expected logical consequences. Thirdly, as a result of the process of adapting the SUMO ontology, we discovered a basic design problem of the ontology which impedes its appropriate use with first order theorem provers. Consequently, we also propose a new transformation to overcome this limitation. As a result of this process, we obtain a validated first-order version of the ontology to be used by first-order theorem provers.
	
Site Oficial: http://adimen.si.ehu.es/web/adimenSUMO

# Related resources:
[2017/05] Mapping between Adimen-SUMO v2.6 and WordNet v3.0: [AS26WN30Mapping.zip] [AS26WN30MappingTools.zip]




# Aterações, a estrutura de organização dos dados foi simplificada e compactada em um unico arquivo. 
