#!/bin/sh

# Maintainer: RodriguesFAS <franciscosouzaacer@gmail.com>
# Description: Deep NLP DashBoard
# Date: 16/01/2019



############################################### STAP
echo "Install Software.."

#echo "Install Lang"
#apt-get install language-pack-ru-base && \
#locale-gen en_US en_US.UTF-8

#echo "Install Java 8"

# Install Java in Deepin OS
# apt-cache search openjdk && \
# apt-get install openjdk-8-jre openjdk-8-jdk && \
# java -version && \
# echo "JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/" >> /etc/environment && \
# source /etc/environment && \
# echo $JAVA_HOME

# Install Java in Debian..

#apt-get install software-properties-common && \
#add-apt-repository ppa:webupd8team/java && \
#apt-get update && \
#su && \
#echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
#exit && \
#apt-get install oracle-java8-installer

echo "Install Unzip.."
apt-get install unzip

echo "Install pip"
apt install python-pip

echo "Install Git"
apt install git



############################################### STAP 
echo "Creating a directory to save external nlp tools.."
mkdir -p ~/tool-nlp
cd tool-nlp



############################################### STAP
echo "Install Dependencies FreeLing.."
apt-get install build-essential
apt-get install cmake
apt-get install swig
apt-get install libboost-all-dev
apt-get install libicu-dev
apt-get install zlib1g-dev

echo "Download FreeLing.."
wget https://github.com/TALP-UPC/FreeLing/releases/download/4.1/FreeLing-4.1.tar.gz

echo "Uncompressed file FreeLing.."
tar -vzxf FreeLing-4.1.tar.gz
rm -r FreeLing-4.1.tar.gz

echo "Install FreeLing.."
cd FreeLing-4.1 && \
mkdir build && \
cd build && \
cmake .. -DPYTHON2_API=ON && \
make -j 4 install



############################################### STAP
echo "Download Stanford CoreNLP.."
cd ~ && \
cd tool-nlp && \
wget http://nlp.stanford.edu/software/stanford-corenlp-full-2018-10-05.zip 

echo "Uncompressed file Stanford CoreNLP.."
unzip stanford-corenlp-full-2018-10-05.zip
rm -r stanford-corenlp-full-2018-10-05.zip



############################################### STAP
echo "Download and Install Dependecies SEMAFOR.."
wget https://github.com/Noahs-ARK/semafor/archive/master.zip

echo "Uncompressed SEMAFOR.."
unzip master && \
mv semafor-master semafor
rm -r master.zip

echo "Config SEMAFOR.."
cd semafor/bin
rm -r config.sh
echo -e '#!/bin/sh \
        \n\nexport BASE_DIR="/home/'.$USER.'/tool-nlp" \
        \nexport SEMAFOR_HOME="${BASE_DIR}/semafor" \
        \nexport CLASSPATH=".:${SEMAFOR_HOME}/target/Semafor-3.0-alpha-04.jar" \
        \nexport JAVA_HOME_BIN="/usr/lib/jvm/java-8-oracle/bin" \
        \nexport MALT_MODEL_DIR="${BASE_DIR}/semafor/models/semafor_malt_model_20121129" \
        \nexport TURBO_MODEL_DIR="{BASE_DIR}/semafor/models/turbo_20130606" \
        \n\necho "Environment variables:" \
        \necho "SEMAFOR_HOME=${SEMAFOR_HOME}" \
        \necho "CLASSPATH=${CLASSPATH}" \
        \necho "JAVA_HOME_BIN=${JAVA_HOME_BIN}" \
        \necho "MALT_MODEL_DIR=${MALT_MODEL_DIR}" \
        '>> config.sh

echo "Download Models SEMAFOR.."
cd .. && \
mkdir -p models && \
cd models && \
wget http://www.ark.cs.cmu.edu/SEMAFOR/semafor_malt_model_20121129.tar.gz && \
tar -vzxf semafor_malt_model_20121129.tar.gz && \
rm -r semafor_malt_model_20121129.tar.gz 

echo "install Maven.."
apt install maven

echo "SEMAFOR Create Packege Maven.."
cd ..
mvn package



############################################### STAP
echo "Install Dependencies Python Project Requirements.."
cd ~

# conda install -c conda-forge spacy -y && \
pip install spacy && \

# python -m spacy download en -y && \
python -m spacy download en && \

# conda install -c menpo pathlib -y && \ 
pip install pathlib && \

pip install xmlformatter && \

# conda install -c anaconda flask -y && \

# conda install -c auto flask-socketio -y && \
pip install flask_socketio && \

# conda install -c anaconda nltk -y && \
pip install nltk && \
python -c "import nltk; nltk.download('all')" && \

# conda install -c kabaka0 stanford-corenlp-python && \
pip install stanfordcorenlp && \

pip install supwsd



############################################### STAP
echo "Download NLP DashBoard ToolKit.."
mkdir -p ~/dash-nlp && \
cd dash-nlp
git clone https://RodriguesFAS@bitbucket.org/RodriguesFAS/dash-cnlp.git


############################################### STAP
echo "Install Completed!"



############################################### STAP
echo "Run WebServer"
cd dash-nlp
echo "http://127.0.0.1:5000"
echo "Strop Ctrl+c"
python run_server.py
