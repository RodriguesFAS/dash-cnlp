$(function () {
    $('#submit').click(function () {
        event.preventDefault();
        var form_data = new FormData($('#form-telegram')[0]);

        $.ajax({
            type: 'POST',
            url: '/save_configurations',
            data: form_data,
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (data, textStatus, jqXHR) {
            alert('OK')
            swal("Success!", "Date Save!", "success");
        }).fail(function (data) {
            swal("Ops!", "Err! Please try again!", "error");
        });
        
    });
}); 