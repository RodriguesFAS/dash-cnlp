$(document).ready(function() {

	$('form').on('submit', function(event) {

		$.ajax({
			data : {
				// dataset
				name_dataset : $('#name_dataset').val(),

				// pre-prossesing
				segmentation_tokenization : $('#segmentation_tokenization').val(),
				word_tokenization : $('#word_tokenization').val(),
				remove_stop_words : $('#remove_stop_words').val(),
				removing_special_characters : $('#removing_special_characters').val(),
				remove_extra_spacing : $('#remove_extra_spacing').val(),
				check_grammar : $('#check_grammar').val(),
				spell_check : $('#spell_check').val(),
				
				// activity 
				lemmatization : $('#lemmatization').val(),
				stemming : $('#stemming').val(),
				length : $('#length').val(),
				parts_of_speech : $('#parts_of_speech').val(),
				ner_corenlp : $('#ner_corenlp').val(),
				ner_spacy : $('#ner_spacy').val(),
				shape : $('#shape').val(),
				morp_type : $('#morp_type').val(),
				true_case : $('#true_case').val(),
				gazzetter : $('#gazzetter').val(),
				shallow_parse : $('#shallow_parse').val(),
				wsd_freeling : $('#wsd_freeling').val(),
				wsd_supwsd : $('#wsd_supwsd').val(),
				frame_semantic_parse : $('#frame_semantic_parse').val(),
				srl_cogcomp : $('#srl_cogcomp').val(),
				svo_srl_freeling : $('#svo_srl_freeling').val(),
			},
			type : 'POST',
			url : '/cnlp'
		})
		.done(function(data) {

			if (data.error) {
				$('#errorAlert').text(data.error).show();
				$('#successAlert').hide();
			}
			else {
				$('#successAlert').text(data.svo_srl_freeling).show();
				$('#errorAlert').hide();
			}

		});

		event.preventDefault();

	});

});