"""
    Software: testsMyFreeLing
    Description: Tests
    Date: 30/08/2018
"""

import json
import tool.MyFreeLing as freeling

# process input sent.
# The big cat eats fresh fish. 
sentence = "The big cat eats fresh fish ."

## synsets
# json_data = freeling.get_synsets(freeling.analyze(sentence))

## pos tag
# json_data = freeling.get_pos_tag(freeling.analyze(sentence))

'''
    sense wsd wordnet
'''
json_data = freeling.get_wsd(freeling.analyze(sentence))
print json_data

## other analyse
# json_data = freeling.get_other_analyse(freeling.analyze(sentence))
#for data in json_data:
 #   print data['synset']
 #   print data['word']


'''
    extrat svo triples srl
'''
'''
json_data = freeling.get_extrat_svo_triples_srl(freeling.analyze(sentence))

if json_data != None:
    for data in json_data['svo']:
        print data, json_data['svo'][data]['lemma'], json_data['svo'][data]['synset']
else:
    print json_data
'''

## Ontology
# print freeling.get_ontologia_class(freeling.analyze(sentence))