"""
    File: tests MyVerbNet
    Tutorial Base: Verbnet Corpus Reader
    Link: http://www.nltk.org/howto/corpus.html#verbnet-corpus-reader
    Date: 21/09/2018
"""
import tool

verbnet = tool.MyVerbNet()

'''
    Make sure we're picking up the right number of elements:
'''
# print "Verbnet statistics:", verbnet.get_statistics()

'''
    Recuperar o sentido no VerbNet, usando o sentido do WordNet.

    Ex. input: lead%2:38:01                   (wordnet)
               have%2:42:00 *nofound
               be%2:42:03 *nofound
               give%2:40:03
               decline%2:32:00 *no found

        output: accompany-51.7                (verbnet)
'''
# print verbnet.get_classid_to_wordnetid('decline%2:32:00')


'''
    Selecting classids based on various selectors:
'''
# print verbnet.get_classids_to_lemma('take') # accept, take, render
# print verbnet.get_classids_to_fileid('approve-77.xml')
# admire-31.2
# give-13.1-1
print verbnet.get_classids_to_classid('give-13.1')

'''
    functions lemmas
'''
# print verbnet.get_lemmas()
# print verbnet.get_lemmas('give-13.1-1')
# print verbnet.get_interval_lemmas(20, 25)
# print len(verbnet.get_lemmas())

'''
'''
# print verbnet.get_vnclass_tree('remove-10.1')
# print verbnet.get_vnclass('give-13.1')
# print verbnet.get_vnclass('admire-31.2')


'''
    get_frames
'''
# print verbnet.get_frames('give-13.1-1')

'''
json_frames = verbnet.get_frames('give-13.1-1')

print '<frames>'
for data_frames in json_frames:
    print '<frame>'
    
    print '<syntax>'
    for element in data_frames['syntax']:
        print '<element>'
        print '<pos_tag out="{pos_tag}"/>'.format(pos_tag=element['pos_tag'])
        print '<modifiers synrestrs="{synrestrs}" selrestrs="{selrestrs}" value="{value}"/>'.format(synrestrs=element['modifiers']['synrestrs'], selrestrs=element['modifiers']['selrestrs'], value=element['modifiers']['value'])
        print '</element>'
    print '</syntax>'

    print '<example out="{example}"/>'.format(example=data_frames['example'])

    print '<semantics predicate_value="{predicate_value}">'.format(predicate_value=data_frames['semantics'][0]['predicate_value'])
    print '<arguments>'
    for argument in data_frames['semantics']:
        print '<argument type="{type}" value="{value}"/>'.format(type=argument['arguments'][0]['type'], value=argument['arguments'][0]['value'])
    print '</arguments>'
    print '</semantics>'

    print '<description primary="{primary}" secondary="{secondary}"/>'.format(primary=data_frames['description']['primary'], secondary=data_frames['description']['secondary'])
    print '</frame>'

print '</frames>'
'''

#print verbnet.get_pprint_frames('give-13.1-1')

'''
    get_subclasses
'''
# print verbnet.get_subclasses('admire-31.2')


'''
    get_themroles
'''
# print verbnet.get_themroles('give-13.1')


'''
    The Verbnet corpus also provides a variety of pretty printing functions that can be used
    to display the xml contents in a more concise form. The simplest such method is pprint():
'''
# print verbnet.get_pprint("admire-31.2")


'''
    get_members
'''
# print verbnet.get_members('admire-31.2')


'''
json_frames = verbnet.get_frames(_classid)
if json_frames:
    data_temp.write('<frames>')
    for data_frames in json_frames:
        data_temp.write('<frame>')
        data_temp.write('<syntax>')
        for element in data_frames['syntax']:
            data_temp.write(
                '<element>')
            data_temp.write('<pos_tag out="{pos_tag}"/>'.format(
                pos_tag=element['pos_tag']))
            data_temp.write('<modifiers synrestrs="{synrestrs}" selrestrs="{selrestrs}" value="{value}"/>'.format(
                synrestrs=element['modifiers']['synrestrs'], selrestrs=element['modifiers']['selrestrs'], value=element['modifiers']['value']))
            data_temp.write(
                '</element>')
        data_temp.write('</syntax>')
        data_temp.write(
            '<example out="{example}"/>'.format(example=data_frames['example']))
        data_temp.write('<semantics predicate_value="{predicate_value}">'.format(
            predicate_value=data_frames['semantics'][0]['predicate_value']))
        data_temp.write('<arguments>')
        for argument in data_frames['semantics']:
            data_temp.write('<argument type="{type}" value="{value}"/>'.format(
                type=argument['arguments'][0]['type'], value=argument['arguments'][0]['value']))
        data_temp.write('</arguments>')
        data_temp.write('</semantics>')
        data_temp.write('<description primary="{primary}" secondary="{secondary}"/>'.format(
            primary=data_frames['description']['primary'], secondary=data_frames['description']['secondary']))
        data_temp.write('</frame>')
    data_temp.write('</frames>')
'''
