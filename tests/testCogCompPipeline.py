'''
    File: testsCogComp
    Description: Execute Function
    Date: 04/09/2018
    Github: https://github.com/CogComp/cogcomp-nlp/tree/master/pipeline
    https://pypi.org/project/dpath/1.4.0/
'''

import json
import tool
import dpath.util

cogcomp = tool.MyCogComp()

#sentence = 'A squirrel is storing a lot of nuts to prepare for a seasonal change in the environment. '

'''
    view:
        POS
        LEMMA
        PARSE_STANFORD
        SHALLOW_PARSE
        NER_CONLL
        SRL_VERB
        SRL_PREP
        SRL_NOM
'''

sentence = "This industry and way of life , however , is in trouble as salmon decline due to habitat degradation from dams , clearcutting , and overgrazing along streams . "

# get view names functions.
# print cogcomp.get_names_functions()

# get data json processing.
# print cogcomp.get_pipeline(sentence, _views)

'''
x = {
    "a": {
        "b": {
        "3": 2,
        "43": 30,
        "c": [],
        "d": ['red', 'buggy', 'bumpers'],
        }
    }
}

print dpath.util.get(x, '/a/b/43')
'''

# print json_data
# print type(json_data)
'''
views = ['SRL_VERB', 'SRL_PREP', 'SRL_NOM']
# views = ['SRL_NOM']

for view in views:
    json_data_srl = cogcomp.server_web(sentence, view)

    #print json_data_srl

    for data_srl in json_data_srl['views']:
        if data_srl['viewName'] == view:
            if data_srl['viewData'][0]['constituents']:
                print '<'+view.lower()+'>'
                
                print '<constituents>'
                for data_srl in json_data_srl['views']:
                    if data_srl['viewName'] == view:
                        cont_constituent = 0
                        for constituent in data_srl['viewData'][0]['constituents']:
                            if len(constituent) > 4:
                                print '<arg constituent="{constituent}" label="{label}">'.format(constituent=cont_constituent, label=constituent['label'])
                                print '<tokens>'
                                for token in json_data_srl['tokens'][constituent['start']:constituent['end']]:
                                    print '<token t_id="t_{t_id}" token="{token}"/>'.format(t_id=constituent['start'], token=token)
                                print '</tokens>'
                                print '<properties SenseNumber="{SenseNumber}" predicate="{predicate}"/>'.format(
                                    SenseNumber=constituent['properties']['SenseNumber'], predicate=constituent['properties']['predicate'])
                                print '</arg>'
                            else:
                                if constituent['label'] != "":
                                    print '<arg constituent="{constituent}" label="{label}">'.format(constituent=cont_constituent, label=constituent['label'])
                                    print '<tokens>'

                                    t_id = constituent['start'] # control token id .

                                    for token in json_data_srl['tokens'][constituent['start']:constituent['end']]:
                                        print '<token t_id="t_{t_id}" token="{token}"/>'.format(t_id=t_id, token=token)
                                        t_id += 1
                                    
                                    print '</tokens>'
                                    print '</arg>'
                            cont_constituent += 1
                print '</constituents>'

                # check is relations
                for data_srl in json_data_srl['views']:
                    if data_srl['viewName'] == view:
                        try:
                            if data_srl['viewData'][0]['relations']:
                                print '<relations>'
                                for data_srl in json_data_srl['views']:
                                    if data_srl['viewName'] == view:
                                        for relations in data_srl['viewData'][0]['relations']:
                                            print '<relation srcConstituent="{srcConstituent}" role="{relationName}" targetConstituent="{targetConstituent}"/>'.format(
                                                relationName=relations['relationName'], srcConstituent=relations['srcConstituent'], targetConstituent=relations['targetConstituent'])
                                print '</relations>'
                        except Exception as err:
                            print "Cogcomp "+view+"  Exception occurred: There are no relationships in sentence. "

                print '</'+view.lower()+'>'
'''
# ----------------------------------------------------------------------------------------


'''
SHALLOW_PARSE
sentence = "As he likes to make people happy, he gives him his wallet."
json_data = cogcomp.server_web(sentence, "SHALLOW_PARSE")

for data in json_data['views']:
    if data['viewName'] == "SHALLOW_PARSE":
        print '<chunkings>'
        ck_id=0
        for constituent in data['viewData'][0]['constituents']:
            if constituent['label'] != '':
                ck_id+=1 
                print '<chunk ck_id="{ck_id}" s_id="s_1" type="{type}">'.format(ck_id=ck_id, type=constituent['label'] )
                t_id=0
                core=0
                # pega os tokens dentro de um intervalo.
                for token in json_data['tokens'][ constituent['start']:constituent['end'] ]:
                    t_id+=1
                    core+=1
                    if core != len(json_data['tokens'][ constituent['start']:constituent['end'] ]):
                        print '<token t_id="t_{t_id}" string="{token}"/>'.format(t_id=t_id, token=token )
                    else:
                        print '<token t_id="t_{t_id}" string="{token}" core{type}="true"/>'.format(t_id=t_id, token=token, type=constituent['label'])
                print '</chunk>'
        print '</chunkings>'

    print '<relations>')
    for data_srl in json_data_srl['views']:
        if data_srl['viewName'] == view:
            for relations in data_srl['viewData'][0]['relations']:
                print '<token srcConstituent="{srcConstituent}" role="{relationName}" targetConstituent="{targetConstituent}" />'.format(
                    relationName=relations['relationName'], srcConstituent=relations['srcConstituent'], targetConstituent=relations['targetConstituent']))
    print '</relations>')
'''
