#!/usr/bin/env python
# -*- coding: utf-8 -*-
import tool

wordnet = tool.MyWordNet()

# 10285313-n = male_child.n.01
# 01629000-v = render.v.04
# print wordnet.get_offset_pos_key('01629000-v')

# print wordnet.get_synset('dog.n.01')
print wordnet.get_synsets('dog')
# print wordnet.get_hypernyms('dog.n.01')

#print 1, wordnet.get_hyponyms('dog.n.01') 
#print 3, wordnet.get_hyponyms('dog.n.01', 1, 5)
# print 2, wordnet.get_hyponyms('dog.n.01', 5)

# print wordnet.get_synsets_tree('car')
# print wordnet.get_lemma_key('give%2:32:01::')
# print wordnet.get_definition('dog.n.01')
# print wordnet.get_examples('give.v.05', True)
# print wordnet.get_examples('give.v.05', False)
# print wordnet.get_hypernyms('eat.v.01')
# print wordnet.get_all_hypernyms('eat.v.01')

#print 1, wordnet.get_synonyms('love')
#print 2, wordnet.get_synonyms('love', 5)

# man, love, good
# print 1, wordnet.get_antonyms('period')
#print 4, wordnet.get_antonyms('good', 4)


# print wordnet.get_member_holonyms('dog.n.01')
# print wordnet.get_entailments()

#print wordnet.get_hypernym_paths('car.n.01')
#print len(wordnet.get_hypernym_paths('car.n.01'))
#print wordnet.get_hypernym_paths('car.n.01', 0)
