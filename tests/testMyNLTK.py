"""
    Software: testsMyFreeLing
    Description: Tests
    Date: 30/08/2018
"""

import tool

scnlp = tool.MyStanfordCoreNLP()
nltk = tool.MyNLTK()

# process input sent
sent = "The boy gave the frog to the girl."

# print scnlp.word_tokenize(sent)
# print nltk.word_tokenize(sent)

# BIO
result = nltk.bio( nltk.word_tokenize(sent) )

print '<bio_nltk><chunks>'
for index, r in enumerate(result):
    print '<chunk t_id="{t_id}" word="{word}" pos="{pos}" ck="{ck}"/>'.format(t_id=index+1, word=r[0], pos=r[1], ck=r[2] )
print '</chunks></bio_nltk>'
    
  