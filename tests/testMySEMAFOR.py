"""
    Tests
"""

import json
import tool.MySEMAFOR as semafor

#            0  1   2   3  4   5    6    7   8  9      10  11   12   13
sentence = 'Do you want me to hold off until I finish July and August ?'

json_data = semafor.get_parse(sentence, 4)

#with open('/home/rodriguesfas/Mestrado/workspace/lexsedi/tmp/tmp-out-semafor.txt', 'r') as fp:
#    json_data = json.load(fp)

if json_data:

    print '<frame_semantic_parse semafor="3.0 alfa" framenet="1.5">'
    print '<frames>'

    for frame in json_data['frames']:
        
        print '<frame>'

        if frame['target']:
            print '<target id_t="t_{id_t}" token="{token}" name="{name}"/>'.format(id_t=frame['target']['spans'][0]['start'], token=frame['target']['spans'][0]['text'], name=frame['target']['name'])
        
        if frame['annotationSets'][0]['frameElements']: #
            print '<frame_elements>'

            for element in frame['annotationSets'][0]['frameElements']:
                quant_element = element['spans'][0]['end'] - element['spans'][0]['start']
                
                if quant_element == 1:
                    print '<element id_t="t_{id_t}" token="{text}" name="{name}"/>'.format(id_t=element['spans'][0]['start'], text=element['spans'][0]['text'], name=element['name'])
                else:
                    print '<element id_t="{id_t}" chunk="{text}" name="{name}"/>'.format(id_t=[ t_id for t_id in range(element['spans'][0]['start'], element['spans'][0]['end'] ) ], text=element['spans'][0]['text'], name=element['name'])
            
            print '</frame_elements>'

        print '</frame>'

    print '</frames>'
    print '</frame_semantic_parse>'