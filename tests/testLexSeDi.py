import tool

lsd = tool.LexSeDi()

"""
    get_morf_type
"""
# word
#print lsd.get_morf_type('RodriguesFAS')
#print lsd.get_morf_type('rodrigues')

# numeric
#print lsd.get_morf_type('884')
#print lsd.get_morf_type('8.84')
#print lsd.get_morf_type('8,84')

# alphnumeric
#print lsd.get_morf_type('Rodrigues2019')
#print lsd.get_morf_type('Rodr1gues')

# simb
#print lsd.get_morf_type('Rodrigue$')
#print lsd.get_morf_type('Rodrigue.')
#print lsd.get_morf_type(';')

"""
    validate_xml
"""
#print lsd.validate_xml()

print lsd.run()