"""
    Software: testsMyFreeLing
    Description: Tests
    Date: 30/08/2018
"""

import tool

babelfy = tool.MyBabelFy()

# process input text
text = "European authorities fined Google a record $5.1 billion on Wednesday for abusing its power in the mobile phone market and ordered the company to alter its practices"

print babelfy.disambiguate(text)



'''
    Analyze: WSD
    Tool: BabelFy
'''
'''
try:
    all_babelfy = babelfy.disambiguate(sentence)
    data_temp.write(
        '<babelfy s_id="s_{s_id}">'.format(s_id=s_id))
    for x_bf in all_babelfy:
        data_temp.write('<token babelSynsetID="{babelSynsetID}" DBpediaURL="{DBpediaURL}" BabelNetURL="{BabelNetURL}" score="{score}" coherenceScore="{coherenceScore}" globalScore="{globalScore}" source="{source}"/>'.format(
            babelSynsetID=x_bf['babelSynsetID'], DBpediaURL=x_bf['DBpediaURL'], BabelNetURL=x_bf['BabelNetURL'], score=x_bf['score'], coherenceScore=x_bf['coherenceScore'], globalScore=x_bf['globalScore'], source=x_bf['source']))
    data_temp.write('</babelfy>')
except Exception as e:
    print 'BabelFy - Error occurred : ' + str(e)
'''