
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tool

gate = tool.MyGate()

# The boy gave an apple to the girl .

print gate.get_category('Metallica')
print gate.get_category('The')
print gate.get_category('boy')
print gate.get_category('gave')
print gate.get_category('an')
print gate.get_category('apple')
print gate.get_category('to')
print gate.get_category('the')
print gate.get_category('girl')
print gate.get_category('.')
print gate.get_category('ica')
print gate.get_category('god')

