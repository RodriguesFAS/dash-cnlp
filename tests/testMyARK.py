import tool

ark = tool.MyARK()

sentence = "The boy gave the frog to the girl ."
json_data = ark.get_parse(sentence)

for data in json_data['sentences']:
    # print data['text']
    # print data['tokens']
    print '<frames>'
    for frame in data['frames']:
        print '<frame>'

        if frame['target']:
            print '<target name="{name}" text="{text}"/>'.format(name=frame['target']['name'], text=frame['target']['text'])

        if frame['annotationSets']:
            print '<frame_elements>'
            for element in frame['annotationSets'][0]['frameElements']:
                print '<element text="{text}" name="{name}"/>'.format(text=element['text'], name=element['name'])
            print '</frame_elements>'
        
        print '</frame>'
        print ''
    print '</frames>'


'''
    Analyze: Frame Semantic Parse
    Tool: ARK (online)
'''
'''
try:
    json_data = ark.get_parse(sentence)
    if json_data:
        for data in json_data['sentences']:
            data_temp.write('<framenet versionID="1.7">')
            data_temp.write('<frames>')
            for frame in data['frames']:
                data_temp.write('<frame>')
                
                if frame['target']:
                    data_temp.write('<target token="{token}" name="{name}"/>'.format(token=frame['target']['text'], name=frame['target']['name']))
                
                if frame['annotationSets']:
                    data_temp.write('<frame_elements>')
                    for element in frame['annotationSets'][0]['frameElements']:
                        data_temp.write('<element token="{text}" name="{name}"/>'.format(text=element['text'], name=element['name']))
                    data_temp.write('</frame_elements>')

                data_temp.write('</frame>')
            data_temp.write('</frames>')
            data_temp.write('</framenet>')
except Exception as err:
    log.logging.error("ARK Exception occurred", exc_info=True)
'''