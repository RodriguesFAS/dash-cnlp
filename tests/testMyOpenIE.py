import util.pprocessing as pp
import tool

openie = tool.MyOpenIE()

sentence = ""

# OpenIE.
try:
    json_data_openie = pp.openie_format_out(
        openie.get_openie(sentence, False))
    if json_data_openie:
        print '<openie s_id="s_{s_id}" openie="{openie}" />'.format(s_id=1, openie=json_data_openie)
except Exception as e:
    print 'OpenIE - Error occurred : ' + str(e)