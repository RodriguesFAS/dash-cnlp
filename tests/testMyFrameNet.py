#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Software: testsFrameNet
    Description: Tests
    Date: 15/09/2018
    Tutorial:
        http://naacl.org/naacl-hlt-2015/tutorial-framenet-data/FrameNetAPI.pdf
"""

import tool

framenet = tool.MyFrameNet()

# retonar uma lista contendo os Frames no FrameNet.
#print "Lista de Frames:", framenet.list_frames()

# retonar a quantidade de Frames no FrameNet.
#print "Quant. de Frames:", len(framenet.list_frames())

'''
    Example 1: “noise” frames
'''
# print framenet.get_frames('noise')

## print framenet.get_list_frames('give')
## print framenet.get_list_frames_plus('give')

'''
    Example 2: Noise_makers
'''
# Motion_noise
# Noise_makers
# print framenet.get_frame('Noise_makers')
# f = framenet.get_frame('Noise_makers')
# print f.FE
## print f.FE['Noise_maker'].definition
## print f.lexUnit.keys()

'''
    Ex 3: “location” FEs
'''

'''
    Ex 4: frame relations
'''
print framenet.get_frame_relations('Causative_of')