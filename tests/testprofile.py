'''
    File: profile.py
    Desciption: 
                This script performs a code analysis to generate a performance statistic.
                Esse script realiza uma análise no código gerando uma estatistica de desempenho.
    Site: https://docs.python.org/3/library/profile.html
    Date: 02/11/2018
'''

import cProfile
import pstats
import runAnnotation as my_slow_module

cProfile.run('my_slow_module.run()', 'restats')
p = pstats.Stats('restats')
p.sort_stats('cumulative').print_stats(30)