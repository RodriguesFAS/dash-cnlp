#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Software: testsMySupWSD
    Description: Tests
    Date: 12/09/2018
"""

import tool

supwsd = tool.MySupWSD()

# process input text
sent = "Do you want me to hold off until I finish July and August ?"

json_data_supwsd = supwsd.disambiguate(sent)

print json_data_supwsd

# show dta json.
# print json_data_supwsd

# generate XML.
print '<supwsd>'
for data_supwsd in json_data_supwsd:
    print '<item_supwsd word="{word}" lemma="{lemma}" pos="{pos}" sense="{sense}" />'.format(
        word=data_supwsd['word'], lemma=data_supwsd['lemma'], pos=data_supwsd['pos'], sense=data_supwsd['sense'])
print '</supwsd>'
