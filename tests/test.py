"""
    Software: Lexsedi
    Description: Test
    Date: 21/07/2018
"""

import config
import tool
import json

from pprint import pprint

my_lsd = tool.LexSeDi()
my_scnlp = tool.MyStanfordCoreNLP()

# doc = my_lsd.load_data(config.PATH['path_file_in']+'/'+config.FILE['input'])

with open(config.PATH['path_file_out']+'/out.annotation.txt') as file:
    doc = json.load(file)
    # print type(data)
    # print data.keys()
    for sentence in doc['sentences']:
        for token in sentence['tokens']:
            print 'index="{index}" word="{word}" '.format(index=token['index'], word=token['word'])

# data = my_scnlp.get_annotate(sentence)
# print type(doc)
# print data

# my_lsd.test()

# tree(pathlib.Path.cwd())

