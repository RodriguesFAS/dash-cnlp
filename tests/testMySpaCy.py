"""
    Software: testsMyFreeLing
    Description: Tests
    Date: 30/08/2018
"""

import tool

spacy = tool.MySpaCy()

# process input text
sentence = "Apple is looking at buying U.K. startup for $1 billion."

'''
    NER
'''
# print spacy.get_ner(sentence)

'''
    BIO
'''
'''
result = spacy.bio(text)

for r in result:
    print r[1]
'''

'''
    Dependency
'''
print spacy.get_dep(sentence)

'''
chunkings = spacy.chunk_verb(sentence, 1)
# loop chunkungs.
for chunk_verb in chunkings:
    ck_id += 1
    data_temp.write('<chunking ck_id="ck_{ck_id}" s_id="s_{s_id}" type="" >'.format(
        ck_id=ck_id, s_id=s_id))
    data_temp.write('<tokens>')
    tokens = chunk_verb.split(" ")
    t_cont = 0
    for token in tokens:
        t_cont += 1
        if t_cont != len(tokens):
            data_temp.write(
                '<token t_id="t_" string="{string}" />'.format(string=token))
        else:
            data_temp.write(
                '<token t_id="t_" string="{string}" coreVP="true"/>'.format(string=token))
    data_temp.write('</tokens>')
    data_temp.write('</chunking>')
'''