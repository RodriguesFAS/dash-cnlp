import os
from ConfigParser import SafeConfigParser

basedir = os.path.abspath(os.path.dirname(__file__))

def write(value1, value2, value3):
    config = SafeConfigParser()
    config.read('settings.cfg')
    config.add_section('telegram')

    config.set('telegram', 'send_msg', value1)
    config.set('telegram', 'telegram_token', value2)
    config.set('telegram', 'telegram_chat_id', value3)

    with open(basedir+'/config/settings.cfg', 'w') as file:
        config.write(file)



def read(config_name, key):
    config = SafeConfigParser()
    config.read(basedir+'/config/settings.cfg')
    return config.get(config_name, key)


if __name__ == "__main__":
    write('True', '793078363:AAFs0Ie_-1_iLw9GGpJ4vnihbWo4K77aiLY', '604669808')

    print read('telegram', 'send_msg')
    print read('telegram', 'telegram_token')
    print read('telegram', 'telegram_chat_id')
