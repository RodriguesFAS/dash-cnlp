#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
    File: runServerCogComp
    Description: Start Server.
    Date: 04/09/2018
    Github: https://github.com/CogComp/cogcomp-nlp/tree/master/pipeline
'''

import tool

cogcomp = tool.MyCogComp()

# start local server.
print ' ├── Starting Server CogComp..'
cogcomp.start_server_local()