'''
    File: load-data
    Description:
    Date: 28/08/2018
'''


def load_data(path):
    data = []
    with open(path) as file:
        for line in file:
            # data.append(unicode(line.strip().decode('utf8')))
            data.append(line.strip())
    return data

if __name__ == "__main__":
    print load_data('/home/rodriguesfas/Mestrado/workspace/dash-cnlp/dataset/input/dataset_test_4_sentences_join.txt')    