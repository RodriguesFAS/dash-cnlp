"""
    File: twillio.py
    Description:
    Tutorial: https://www.idiotinside.com/2018/08/17/send-message-whatsapp-api-python-twilio/
    Date: 01/11/2018
"""

import config.settings as config
import requests


TWILIO_SID = config.TWILIO['TWILIO_SID']
TWILIO_AUTHTOKEN = config.TWILIO['TWILIO_AUTHTOKEN']
TWILIO_NUMBER = config.TWILIO['TWILIO_NUMBER']
TWILIO_MESSAGE_ENDPOINT = "https://api.twilio.com/2010-04-01/Accounts/{TWILIO_SID}/Messages.json".format(TWILIO_SID=TWILIO_SID)


def send_whatsapp_message(message):
    '''
        Send Whatsapp Message
        @param message
    '''
    message_data = {
        "To": config.TWILIO['TO_NUMBER'],
        "From": TWILIO_NUMBER,
        "Body": 'LexSeDi: ' + message,
        }

    if config.TWILIO['SEND_MSG']:        
        response = requests.post(TWILIO_MESSAGE_ENDPOINT, data=message_data, auth=(TWILIO_SID, TWILIO_AUTHTOKEN))
        return response.json()

# Test.
if __name__ == '__main__':
    appointment_msg = """Your appointment is coming up on August 20th at 6:00PM""" 
    msg = send_whatsapp_message(appointment_msg)

    print(msg['sid']) # SM5xxxafa561e34b1e84c9d22351ae08a0
    print(msg['status']) # queued