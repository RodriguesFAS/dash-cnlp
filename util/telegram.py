'''
    Date: 02/11/2018
    Tutorial: https://www.marcodena.it/blog/telegram-logging-handler-for-python-java-bash/
'''

import config.settings as config
import requests
import logging
import datetime
from logging import Handler, Formatter

def send_message(message):
    '''
    '''
    payload = {
        'chat_id': config.TELEGRAM['TELEGRAM_CHAT_ID'],
        'text': message,
        'parse_mode': 'HTML'
        }

    if config.TELEGRAM['SEND_MSG']:
        return requests.post("https://api.telegram.org/bot{token}/sendMessage".format(token=config.TELEGRAM['TELEGRAM_TOKEN']), data=payload).content

# Test
if __name__ == '__main__':
    send_message('Haaaa!')