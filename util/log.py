"""
    Description:
    Date: 02/11/2018
"""

import config.settings as config
import logging

if config.CONFIG['debug']:
    logging.basicConfig(
        # level=logging.DEBUG,
        filename=config.FILE['log'],
        filemode='w',
        format='%(asctime)s-%(process)d-%(levelname)s-%(message)s \n')