'''
    File: pprocessing
    Description: Pre-processing
    Date: 24/08/2018
'''

import re


def remove_punct(_sent):
    '''
        Remove punct sentence.
    '''
    return "".join(c for c in _sent if c not in ('!', '.', ':', ','))

def stopwords(_sent):
    '''
    '''
    return 'null'


def remove_extra_spacing(_sent):
    '''
        Remove extra spacing.
    '''
    return re.sub(' +', ' ', _sent)


def wn_format_text(_data):
    return "".join(c for c in _data if c not in ("'", '"'))


def openie_format_out(_data):
    return _data.replace('"', "'")
