'''
    File: internet
    Description: 
    Date: 28/08/2018
'''
import socket


'''
    Function: check
    Description: Check connection in internt.
'''
def check():

    url = ['www.google.com']

    for host in url:
        a = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        a.settimeout(.5)
        try:
            b = a.connect_ex((host, 80))
            if b == 0:
                return True
        except:
            pass
        a.close()

    return False
