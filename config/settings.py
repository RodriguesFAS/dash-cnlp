# -*- coding: utf-8 -*-

'''
    File: settings
    Description: 
        In this file, you will find some settings that need to be adjusted before you 
        start using this tool, you will find here things like external NLP tool path 
        configuration and some of the usage settings, check the documentation for more 
        details.
    Date: 03/07/2018
'''

import os
import pathlib

home_user = os.environ['HOME']

PATH = {
    'path_root': pathlib.Path.cwd(),
    'path_file_in': str(pathlib.Path.cwd()) + '/dataset/input/',
    'path_file_out': str(pathlib.Path.cwd()) + '/dataset/output/',
    'path_dir_stanford_corenlp': home_user + '/tool-nlp/stanford-corenlp-full-2018-10-05',
    #'path_dir_stanford_corenlp': '/home/tool-nlp/stanford-corenlp-full-2018-10-05',
    'path_dir_stanford_openie': '#',
    'path_dir_temp': str(pathlib.Path.cwd()) + '/tmp',
    'path_dir_cogcomp_nlp': home_user + '/tool-nlp/cogcomp-nlp/',
    #'path_dir_cogcomp_nlp': '/home/tool-nlp/cogcomp-nlp/',
    'path_dir_gate': str(pathlib.Path.cwd()) + '/resources/dict/gazetteer-gate-8.5.1/',
    'path_dir_wnd': str(pathlib.Path.cwd()) + '/resources/dict/wnd-3.2/',
    'path_dir_asumo': str(pathlib.Path.cwd()) + '/resources/dict/AS26WN30Mapping/',
    'path_dir_semafor': home_user + '/tool-nlp/semafor/',
    #'path_dir_semafor': '/home/tool-nlp/semafor/',
}

FILE = {
    'output': 'out_dataset_annotation.xml',
    'xsd': 'scheme.xsd',
    'temp_tokens': 'tmp-tokens-scnlp.txt',
    'temp_openie': 'tmp-openie.txt',
    'xwnd': str(pathlib.Path.cwd()) + '/resources/dict/xwnd-30g/',
    'log': str(pathlib.Path.cwd()) + '/logs/data.log',
    'tmp_in_semafor': 'tmp-in-semafor.txt',
    'tmp_out_semafor': 'tmp-out-semafor.txt',
}

CONFIG = {
    'stanford_corenlp_memory': "memory='8g'",
    'create_new_file_out': True,
    'internet_connection': True,
    'server': 'http://localhost',
    'cogcomp_port': '8080',
    'debug': True,
}

SELECTION_PRE_PROCESSING = {
    'stopwords_removal': False,
    'special_characters_removal': False,
    'spellcheck': False,
    'remove_punct': False,
    'tokenization_default_corenlp': False,
}

KEYS = {
    'babelfy': '5e962130-b37f-4105-8512-4c97b4f3cb30',
}

LANG = {
    'babelfy': 'EN',
    'bdpediaspotligth': 'en',
    'freeling': 'en',
}

TELEGRAM = {
    'SEND_MSG': True,
    'TELEGRAM_TOKEN': '793078363:AAFs0Ie_-1_iLw9GGpJ4vnihbWo4K77aiLY',
    'TELEGRAM_CHAT_ID': '604669808',
}

MSG = {
    0: 'Mode Tool Local and Online!',
    1: 'You do not have internet connection, configure internet_connection: False in file config.py, to use offline!',
    2: 'Mode Tool Local!',
    3: 'Loading dataset..',
    4: 'Tokenization default dataset..',
    5: 'Loading dataset tokenized..',
    6: 'Processing sentences..',
}

SIMB = {
    0: '🚀',
    1: '└── ',
    2: '├──',
    3: '¯ \ _ (ツ) _ / ¯',
    4: " *      Run LexSeDi ..          \n *  Go have coffee, have a     \n *  message when you're done. \n *                       _____ \n *                    __/_ /// \n *                   / _/    \ \n *                   \/_\=[o=o] \n *                    \_,    __) \n *                     |     _\ \n *                     l______/ \n *                    /     :| \n *                   /  \   ;|- \n *                   \_______j \n *                   ./.....\.. \n ",
}

ABOUT = {
    'software': 'Deep NLP DashBoard ToolKit',
    'description': '',
    'author': 'RodriguesFAS',
    'email': 'franciscosouzaacer@gmail.com',
    'url': '',
    'license': 'MIT',
    'data': '20/01/2019',
    'version': '1.0.0',
}
